package sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.apache.commons.lang.ArrayUtils;

public class Sort {

	static Logger logger = Logger.getLogger(Sort.class.getSimpleName());

	public static void main(String[] args) {

		int[] arr = new int[10000];
		List<Integer> list = new ArrayList<Integer>(100000);
		Random rand = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextInt(10000);
			list.add(arr[i]);
		}
		long startTime = 0;
		long endTime = 0;

		int[] bubbleArr = ArrayUtils.clone(arr);
		startTime = System.currentTimeMillis();
		Sort.bubbleSort(bubbleArr);
		endTime = System.currentTimeMillis();
		System.out.println("Bubble Sort Cost Time : " + (endTime - startTime));
//		System.out.println(Sort.arrayToString(bubbleArr));

		int[] insertArr = ArrayUtils.clone(arr);
		startTime = System.currentTimeMillis();
		Sort.insertionSort(insertArr);
		endTime = System.currentTimeMillis();
		System.out.println("Insert Sort Cost Time : " + (endTime - startTime));
//		System.out.println(Sort.arrayToString(insertArr));

		int[] selectionArr = ArrayUtils.clone(arr);
		startTime = System.currentTimeMillis();
		Sort.selectionSort(selectionArr);
		endTime = System.currentTimeMillis();
		System.out.println("Selection Sort Cost Time : "
				+ (endTime - startTime));
//		System.out.println(Sort.arrayToString(selectionArr));

		// int[] quickArr = ArrayUtils.clone(arr);
		// System.out.println(Sort.arrayToString(quickArr));
		// startTime = System.currentTimeMillis();
		// Sort.quickSort(quickArr, 0, quickArr.length - 1);
		// endTime = System.currentTimeMillis();
		// System.out.println("Quick Sort Cost Time : " + (endTime -
		// startTime));
		// System.out.println(Sort.arrayToString(quickArr));

		// int[] mergeArr = ArrayUtils.clone(arr);
		// startTime = System.currentTimeMillis();
		// Sort.mergeSort(mergeArr, 0, mergeArr.length - 1,
		// new int[mergeArr.length]);
		// endTime = System.currentTimeMillis();
		// System.out.println("Merge Sort Cost Time : " + (endTime -
		// startTime));
		// System.out.println(Sort.arrayToString(mergeArr));

		// startTime = System.currentTimeMillis();
		// Collections.sort(list);
		// endTime = System.currentTimeMillis();
		// System.out.println("Collections Sort Cost Time : " + (endTime -
		// startTime));
		// System.out.println(list.toString());
	}

	/**
	 * 冒泡排序 0~n-1，从左向右依次比较，如果左侧大就和右侧交换，直到比较到最后一个数字，找出最大值 然后对剩下的值做同样的比较操作
	 * 
	 * @param arr
	 */
	public static void bubbleSort(int[] arr) {
		if (ArrayUtils.isEmpty(arr)) {
			return;
		}
		int len = arr.length;
		int temp = 0, j = 0;
		for (int i = 0; i < len; i++) {
			j = 0;
			for (; j < len - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
	}

	/**
	 * 插入排序
	 * 
	 * @param arr
	 * @return
	 */
	public static void insertionSort(int[] arr) {

		if (ArrayUtils.isEmpty(arr)) {
			return;
		}
		int len = arr.length;
		int j = 0, temp = 0;
		for (int i = 1; i < len; i++) {
			temp = arr[i];
			j = i;
			while (j > 0 && arr[j - 1] > temp) {
				arr[j] = arr[j - 1];
				j--;
			}
			arr[j] = temp;
		}
	}

	/**
	 * 选择排序
	 * 
	 * @param arr
	 */
	public static void selectionSort(int[] arr) {

		if (ArrayUtils.isEmpty(arr)) {
			return;
		}
		int len = arr.length;
		int j = 0, min = 0, temp = 0;
		for (int i = 0; i < len - 1; i++) {
			j = i + 1;
			min = i;
			for (; j < len; j++) {
				if (arr[min] > arr[j]) {
					min = j;
				}
			}

			if (i != min) {
				temp = arr[i];
				arr[i] = arr[min];
				arr[min] = temp;
			}
		}
	}

	/**
	 * 快速排序
	 * 
	 * @param arr
	 */
	public static void quickSort(int[] arr, int left, int right) {

		if (ArrayUtils.isEmpty(arr)) {
			return;
		}
		if (left < right) {
			int p = partion(arr, left, right);
			quickSort(arr, left, p - 1);
			quickSort(arr, p + 1, right);

		}
	}

	/**
	 * @param arr
	 * @param left
	 * @param right
	 * @return
	 */
	private static int partion(int[] arr, int left, int right) {
		int pivot = arr[left];
		while (left < right) {
			// 如果右面比p 大，继续判断右面的值，直到找到右面比p小的值
			while (left < right && arr[right] >= pivot) {
				right--;
			}
			// swap(arr, left, right);替换比交换效率高
			if (left < right) {
				arr[left] = arr[right];
			}

			while (left < right && arr[left] <= pivot) {
				left++;
			}
			// swap(arr, left, right);替换比交换效率高
			if (left < right) {
				arr[right] = arr[left];
			}
		}

		arr[left] = pivot;// 最后找到中间值的位置（此时 left = right）
		return left;
	}

	/**
	 * 归并排序
	 * 
	 * @param arr
	 * @param start
	 * @param mid
	 * @param last
	 * @param temp
	 */
	public static void mergeSort(int[] arr, int start, int last, int[] temp) {

		if (start < last) {

			int mid = start + (last - start) / 2;

			System.out.println("--->" + start + "," + mid + "," + last);
			System.out
					.println("------------------!---------------------------");

			mergeSort(arr, start, mid, temp);// 递归排序左侧

			mergeSort(arr, mid + 1, last, temp);// 递归排序右侧

			System.out.println("---!>" + start + "," + mid + "," + last);
			System.out
					.println("------------------!---------------------------!");

			mergeArr(arr, start, mid, last, temp);
		}
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	/**
	 * @param arr
	 * @param start
	 * @param mid
	 * @param last
	 * @param temp
	 */
	private static void mergeArr(int[] arr, int start, int mid, int last,
			int[] temp) {

		int i = start, j = mid + 1, n = last;
		int k = 0;

		while (i <= mid && j <= n) {
			if (arr[i] <= arr[j]) {
				temp[k++] = arr[i++];
			} else {
				temp[k++] = arr[j++];
			}
		}

		System.out.println(Sort.arrayToString(arr));
		while (i <= mid) {
			temp[k++] = arr[i++];
		}
		while (j <= n) {
			temp[k++] = arr[j++];
		}

		for (i = 0; i < k; i++) {
			arr[start + i] = temp[i];
		}

		System.out.println(Sort.arrayToString(temp));
		System.out.println("===================!======================");
	}

	/**
	 * @param arr
	 * @return
	 */
	public static String arrayToString(int[] arr) {

		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int str : arr) {
			sb.append(str).append(",");
		}
		if (sb.length() > 1) {
			sb.deleteCharAt(sb.length() - 1);
		}
		sb.append("]");
		return sb.toString();
	}
}
