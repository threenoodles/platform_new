package collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionTest {

	public static void main(String[] args) {
		
		List<Integer> lst = new ArrayList<Integer>(10);
		lst.add(1);
		lst.add(2);
		lst.add(3);
		lst.add(4);
		lst.add(5);
		lst.add(6);
		lst.add(7);
		
		removeTest(lst);
		System.out.println(lst.size());
	}
	
	public static void removeTest(List<Integer> lst){
		
		for(Integer integer : lst){
			if(integer%2 ==0){
				lst.remove(integer);
			}
		}
	}
	
	public static void removeTest1(List<Integer> lst){
		
		Iterator<Integer> itr = lst.iterator();
		while(itr.hasNext()){
			if(itr.next()%2 == 0){
				itr.remove();
			}
		}
	}
}
