package thread;

/**
 * Thread sleep和wait区别
 * 
 * @author DreamSea 2012-1-15
 */
public class ThreadSleepAndWaitTest implements Runnable {
	int number = 10;

	public void firstMethod() throws Exception {
		synchronized (this) {
			number += 100;
			System.out.println("firstMethod->" + number);
		}
	}

	public void secondMethod() throws Exception {
		synchronized (this) {
//			Thread.sleep(2000);//调用sleep，线程保持着线程锁
			this.wait(2000);//调用wait，线程释放线程锁
			number *= 200;
			System.out.println("secondMethod->" + number);
		}
	}

	@Override
	public void run() {
		try {
			firstMethod();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		ThreadSleepAndWaitTest threadTest = new ThreadSleepAndWaitTest();
		Thread thread = new Thread(threadTest);
		thread.start();

		// 防止在sencodmethod 调用前，firstMethod 方法被调用,如果不加Thread.sleep(100);那结果可能有
		// 110 或者是 2100
		// Thread.sleep(100);
		threadTest.secondMethod();
	}
}