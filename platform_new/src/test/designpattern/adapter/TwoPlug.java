package designpattern.adapter;

/**二相充电器
 * @author chenwenpeng
 *
 */
public class TwoPlug {

	public void charge(){
		
		System.out.println("二相充电器充电中。。。");
	}
}
