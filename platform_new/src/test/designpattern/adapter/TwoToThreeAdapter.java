package designpattern.adapter;

/**二相转三相适配器
 * @author chenwenpeng
 *
 */
public class TwoToThreeAdapter implements ThreePlug{

	private TwoPlug twoPlug;
	public TwoToThreeAdapter(TwoPlug twoPlug){
		
		this.twoPlug = twoPlug;
	}

	@Override
	public void charge() {
		twoPlug.charge();
	}
}
