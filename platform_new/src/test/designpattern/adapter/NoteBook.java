package designpattern.adapter;

/**笔记本
 * 适配器模式将某个类的接口转换成客户端期望的另一个接口表示，目的是消除由于接口不匹配所造成的类的兼容性问题
 * @author chenwenpeng
 *
 */
public class NoteBook {

	private ThreePlug tp;
	public NoteBook(ThreePlug tp){
		this.tp = tp;
	}
	public void charge(){
		tp.charge();
	}
	
	public static void main(String[] args) {
		
		TwoPlug tp = new TwoPlug();
		ThreePlug thp = new TwoToThreeAdapter(tp);
		
		NoteBook noteBook = new NoteBook(thp);
		noteBook.charge();
	}
}
