package designpattern.facede;

public class Flight {

	public void scheduledFlight() {
		System.out.println("预定机票");
	}
	
	public void goTo() {
		System.out.println("去机场");
	}
}
