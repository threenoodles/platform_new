package designpattern.facede;

public class Restaurant {

	public void scheduledRestaurant() {
		System.out.println("预定餐馆");
	}
	
	public void eat() {
		System.out.println("去吃饭");
	}
}
