package designpattern.facede;

/**为调用着提供一个简单的调用接口
 * @author chenwenpeng
 *
 */
public class MYFacede {

	public void goTo(){
		
		Hotel hotel = new Hotel();
		hotel.scheduledHotel();
		Flight flight = new Flight();
		flight.scheduledFlight();
		Restaurant restaurant = new Restaurant();
		restaurant.scheduledRestaurant();
		Play play = new Play();
		play.go();
		play.play();
	}
	
	public void goBack(){
		
		Flight flight = new Flight();
		flight.scheduledFlight();
		flight.goTo();
		this.goHome();
	}
	
	private void goHome(){
		System.out.println("回家");
	}
}
