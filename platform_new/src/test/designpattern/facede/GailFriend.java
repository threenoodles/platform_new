package designpattern.facede;

/**外观模式是为了简化类与类的复杂的依赖关系
 * @author chenwenpeng
 *
 */
public class GailFriend {

	
	public static void main(String[] args) {
		
		MYFacede wo = new MYFacede();
		wo.goTo();
		
		wo.goBack();
	}

}
