package designpattern.decorator;

/**
 * @author chenwenpeng
 *
 */
public interface INaked {
	
	/**
	 * 穿衣服
	 */
	public void dress();
}
