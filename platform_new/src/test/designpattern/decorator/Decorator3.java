package designpattern.decorator;

/**
 * @author chenwenpeng
 *
 */
public class Decorator3 implements INaked{
	
	private INaked naked;
	public Decorator3(INaked naked){
		this.naked = naked;
	}
	
	public void dress(){
		
		System.out.println("准备毛裤。。。");
		naked.dress();
	}
}
