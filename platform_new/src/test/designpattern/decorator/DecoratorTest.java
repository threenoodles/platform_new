package designpattern.decorator;

/**
 * @author chenwenpeng
 *顾名思义，装饰模式就是给一个对象增加一些新的功能，而且是动态的，要求装饰对象和被装饰对象实现同一个接口，装饰对象持有被装饰对象的实例
 */
public class DecoratorTest{
	
	public static void main(String[] args) {
		
		INaked naked = new Naked();
		
		INaked dressObj = new Decorator1(new Decorator2(new Decorator3(new Decorator4(naked))));
		dressObj.dress();
	}
}
