package designpattern.decorator;

/**
 * @author chenwenpeng
 *
 */
public class Decorator1 implements INaked{
	
	private INaked naked;
	public Decorator1(INaked naked){
		this.naked = naked;
	}
	
	public void dress(){
		
		System.out.println("准备秋裤。。。");
		naked.dress();
	}
}
