package designpattern.proxy;

import java.lang.reflect.Proxy;

import designpattern.proxy.dynamic.BugProxy;

/**
 * @author chenwenpeng
 *	代理模式就是多一个代理类出来，替原对象进行一些操作
 */
public class BugProxyTest{
	
	public static void main(String[] args) throws InstantiationException, IllegalAccessException {
		
		System.out.println("----------------------静态代理----------------------------");
		//静态代理
		IBug lehua = new LeHua();
		LeHuaBugProxy lhp = new LeHuaBugProxy(lehua);
		lhp.reportBug();
		
		IBug chenbo = new ChenBo();
		ChenBoBugProxy chenbop = new ChenBoBugProxy(chenbo);
		chenbop.reportBug();
		System.out.println("----------------------动态代理----------------------------");
		//动态代理
		BugProxy proxy = new BugProxy();
		proxy.setObj(LeHua.class.newInstance());
		IBug bugger = (IBug) Proxy.newProxyInstance(
				LeHua.class.getClassLoader(),
				LeHua.class.getInterfaces(), proxy);
		bugger.reportBug();
		
		proxy.setObj(ChenBo.class.newInstance());
		bugger = (IBug) Proxy.newProxyInstance(
				LeHua.class.getClassLoader(),
				LeHua.class.getInterfaces(), proxy);
		bugger.reportBug();
		
	}
}
