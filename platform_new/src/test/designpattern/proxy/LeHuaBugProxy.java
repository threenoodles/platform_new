package designpattern.proxy;

public class LeHuaBugProxy implements IBug{

	private IBug bug;
	public LeHuaBugProxy(IBug bug){
		this.bug = bug;
	}
	@Override
	public void reportBug() {
		
		bug.reportBug();
	}
}
