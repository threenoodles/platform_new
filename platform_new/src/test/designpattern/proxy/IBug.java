package designpattern.proxy;

/**BUG
 * @author chenwenpeng
 *
 */
public interface IBug {
	
	/**
	 * 提交bug
	 */
	public void reportBug();
}
