package designpattern.facede;

public class Hotel {

	public void scheduledHotel() {
		System.out.println("预定宾馆");
	}
	
	public void go() {
		System.out.println("回宾馆");
	}
}
