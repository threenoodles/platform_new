package designpattern.bridge;

/**
 * @author chenwenpeng
 * 桥接，顾名思义，在接口与实现之间，架设一座桥梁，隔离接口与实现
 * 桥接模式允许两层实现的抽象，桥接模式提供了更多的灵活性。
 */
public class RemoteControl extends AbstractRemoteControl {

	public RemoteControl(ITV tv) {
		super(tv);
	}

	public void setChannelKeyboard(int channel) {
		this.setChannel(channel);
	}
	
	public void setClock(int channel) {
		System.out.println("设置闹钟");
	}

	public static void main(String[] args) {
		ITV tv = new SonyTV();
		RemoteControl lrc = new RemoteControl(tv);
		lrc.setChannelKeyboard(100);
	}
}
