package designpattern.decorator;

/**
 * @author chenwenpeng
 *
 */
public class Naked implements INaked{
	
	/**
	 * 穿衣服
	 */
	public void dress(){
		
		System.out.println("正在穿。。。");
	}
}
