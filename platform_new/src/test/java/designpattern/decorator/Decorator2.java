package designpattern.decorator;

/**
 * @author chenwenpeng
 *
 */
public class Decorator2 implements INaked{
	
	private INaked naked;
	public Decorator2(INaked naked){
		this.naked = naked;
	}
	
	public void dress(){
		
		System.out.println("准备秋衣。。。");
		naked.dress();
	}
}
