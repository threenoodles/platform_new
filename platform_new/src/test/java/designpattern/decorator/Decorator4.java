package designpattern.decorator;

/**
 * @author chenwenpeng
 *
 */
public class Decorator4 implements INaked{
	
	private INaked naked;
	public Decorator4(INaked naked){
		this.naked = naked;
	}
	
	public void dress(){
		
		System.out.println("准备毛衣。。。");
		naked.dress();
	}
}
