package designpattern.proxy;

public class ChenBoBugProxy implements IBug{

	private IBug bug;
	public ChenBoBugProxy(IBug bug){
		this.bug = bug;
	}
	@Override
	public void reportBug() {
		
		bug.reportBug();
	}
}
