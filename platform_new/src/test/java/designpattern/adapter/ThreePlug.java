package designpattern.adapter;

/**三相充电器
 * @author chenwenpeng
 *
 */
public interface ThreePlug {

	public void charge();
}
