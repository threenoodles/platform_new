package test;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;


public class Test {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, SecurityException, NoSuchMethodException {

			Set<String> set = new HashSet<String>();
			Set<String> set1 = new TreeSet<String>();
			
			//spring 实例化私有的构造函数的方法
//			ReflectTest.class.getDeclaredConstructor().setAccessible(true);
//			System.out.println(ReflectTest.class.newInstance());
			
			
			ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>(10);
			map.putIfAbsent("test", "1");
//			map.put("test", "2");
			map.put("test1", "3");
			
			System.out.println(map.get("test"));
			System.out.println(map.get("test1"));
	}
}
