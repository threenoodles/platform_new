package test;

public class ReflectTest {

	private int a = 1;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	private ReflectTest() {

	}
}
