package test;


import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.utils.DateTimeUtil;
/**
 * @author chenwenpeng
 * 
 */
@Controller
@RequestMapping(value = "/api")
public class TestCertificationAuthController {
    @SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
//    	
//    	CloseableHttpClient client = null;
//    	
//    	HttpRequestBase post = new HttpPost("http://localhost:8083/newapi/getAcctPoiList");
//    	HttpEntity httpEntry = new BasicHttpEntity();
//    	List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("acctId", "0"));
//        params.add(new BasicNameValuePair("poiId", "-1"));
//        params.add(new BasicNameValuePair("token1", "tokentokentokentoken"));
//        httpEntry = new UrlEncodedFormEntity(params,HTTP.UTF_8);
//        ((HttpPost)post).setEntity(httpEntry);
//        
//        CookieStore cookieStore = new BasicCookieStore();
//        BasicClientCookie cookie = new BasicClientCookie("token","tokentokentokentoken1");
//        cookie.setPath("/");
//        cookieStore.addCookie(cookie);
//        
//        client = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
//        client.execute(post);
//    	CookieStore cookieStore = new BasicCookieStore();

        // Create local HTTP context
        
        // Bind custom cookie store to the local context
        
    	
    	DefaultHttpClient httpclient = new DefaultHttpClient();  
//        // 创建一个本地Cookie存储的实例  
//    	CookieStore cookieStore = new BasicCookieStore();
//	      BasicClientCookie cookie = new BasicClientCookie("token","tokentokentokentoken1");
//	      cookie.setPath("/");
//	      cookieStore.addCookie(cookie);  
//        //创建一个本地上下文信息  
////        HttpContext localContext = new BasicHttpContext();  
//        HttpClientContext localContext = HttpClientContext.create();
//        localContext.setCookieStore(cookieStore);
    	
    	CookieStore cookieStore = new BasicCookieStore();
    	BasicClientCookie cookie = new BasicClientCookie("token","tokentokentokentoken1");
	      cookie.setPath("/");
	      cookie.setExpiryDate(DateTimeUtil.parseTime("2020-01-01 23:59:59", "yyyy-MM-dd"));
	      cookie.setVersion(10);
	      cookie.setSecure(true);
	      cookie.setComment("comment");
	      
	      cookieStore.addCookie(cookie);  
        // Create local HTTP context
        HttpClientContext localContext = HttpClientContext.create();
        // Bind custom cookie store to the local context
        localContext.setCookieStore(cookieStore);

        //在本地上下问中绑定一个本地存储  
//        localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);  
        //设置请求的路径  
        HttpGet httpget = new HttpGet("http://localhost:8083/newapi/getAcctPoiList");
        Header header = new BasicHeader("Set-Cookie", "Set-Cookie1");
        httpget.addHeader(header);
        //传递本地的http上下文给服务器  
        HttpResponse response = httpclient.execute(httpget, localContext);
        System.out.println(response.getStatusLine().getStatusCode());
    	
    }
}
