package thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.collections.CollectionUtils;

public class ReadWriteLockTest {
	private static final List<Double> atm = new ArrayList<Double>(10000); 
	
	private ReadWriteLock rwl = new ReentrantReadWriteLock();
	private final Lock readLock = rwl.readLock();
	private Lock writeLock = rwl.readLock();
	
	
	class Taker1 implements Runnable{
		
		@Override
		public void run() {
			readLock.lock();
			try{
				if(!CollectionUtils.isEmpty(atm) && atm.size() >0){
					System.out.println(atm.remove(0));
				}
			}finally{
				readLock.unlock();
			}
		}
	}
	
	class Taker2 implements Runnable{
		
		@Override
		public void run() {
			readLock.lock();
			try{
				if(!CollectionUtils.isEmpty(atm) && atm.size() >0){
					System.out.println(atm.remove(0));
				}
			}finally{
				readLock.unlock();
			}
		}
	}
	
	class TakerIn implements Runnable{
		@Override
		public void run() {
			writeLock.lock();
			try{
					for(int i = 0;i<100;i++){
						atm.add(Math.random() * 100);
						System.out.println("add one : " + i);
					}
			}finally{
				writeLock.unlock();
			}
		}
	}
	
	public static void main(String[] args) {
		ReadWriteLockTest rwl = new ReadWriteLockTest();
		//ReadWriteLock 写锁的优先级高于读锁；读写锁互斥；写写锁互斥；读锁不互斥
//		new Thread(rwl.new Taker1()).start();
//		new Thread(rwl.new Taker2()).start();
//		new Thread(rwl.new TakerIn()).start();
		
		AtomicInteger ait = new AtomicInteger(10);
		
		System.out.println(ait.addAndGet(10));;
		System.out.println(ait.compareAndSet(20, 30));
		System.out.println(ait.getAndIncrement());
		System.out.println(ait.getAndDecrement());
		
	}
}
