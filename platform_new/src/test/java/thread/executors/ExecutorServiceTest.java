package thread.executors;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {

//		Executors
//		ExecutorService executorService = Executors.newFixedThreadPool(10);
//		executorService.execute(new TestRunnable(0));
//		
//		Future<String> future = executorService.submit(new TestCallable());
//		while(!future.isDone()){
//			System.out.println(future.get());
//		}
//		
//		//自定义线程池
//		BlockingQueue<Runnable> bqueue = new ArrayBlockingQueue<Runnable>(100);//如果任务超过任务队列，那么任务将被抛弃
//		ExecutorService myPool = new ThreadPoolExecutor(2,3,10,TimeUnit.SECONDS,bqueue);
//		
//		for(int i = 0 ;i<100;i++){
//			System.out.println("bqueue.size : " + bqueue.size());
//			myPool.execute(new TestRunnable(i));
//		}
		
		
		ExecutorService scheduledPool = Executors.newScheduledThreadPool(10);
		
		
		ScheduledExecutorService scheduledES = Executors.newScheduledThreadPool(10);
		scheduledES.scheduleAtFixedRate(new TestRunnable(10), 1000, 1000, TimeUnit.MILLISECONDS);
	}
	
}

class TestRunnable implements Runnable{
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public TestRunnable(int id){
		this.id = id;
	}
	public void run() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("task run : " + this.id);
	}
}

class TestCallable implements Callable<String>{

	@Override
	public String call() throws Exception {

		return "call()方法被自动调用，任务返回的结果是：" + Thread.currentThread().getName();   
	}
}
