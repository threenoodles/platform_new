package thread;

/**生产者和消费者
 * @author chenwenpeng
 *
 */
public class CircleAB extends Thread {
	private static int count = 1;
	private String name;
	private Object lock;

	public CircleAB(String name, String lock) {
		this.name = name;
		this.lock = lock;
	}

	@Override
	public void run() {
		super.run();

		while (count ++ <100) {
			synchronized (lock) {
				System.out.println(this.name);
				//激活持有lock锁，并调用wait()的线程，让其进入可执行阶段
				lock.notifyAll();
				try {
					//当前线程等待，释放锁
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		String lock = new String();
		CircleAB cab1 = new CircleAB("A", lock);
		CircleAB cab2 = new CircleAB("B", lock);

		cab1.start();
		Thread.sleep(100);
		cab2.start();
		Thread.sleep(100);
		
	}
}
