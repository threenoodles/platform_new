package thread;

import java.util.concurrent.atomic.AtomicLong;


public class AtomicLongTest {
	public static AtomicLong acl = new AtomicLong();
	public static int i = 0;
	public static void main(String[] args) {
		
		new Thread(new Taker1()).start();
		new Thread(new Taker2()).start();
	}
}

class Taker1 implements Runnable{
	
	@Override
	public void run() {
		for(;;){
			AtomicLongTest.acl.incrementAndGet();
			try {
				Thread.sleep(100);
				AtomicLongTest.i ++;
//				System.out.println("task1 i : " + AtomicLongTest.i);
				System.out.println("task1 acl : " + AtomicLongTest.acl.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class Taker2 implements Runnable{
	
	@Override
	public void run() {
		for(;;){
			AtomicLongTest.acl.incrementAndGet();
			try {
				AtomicLongTest.i ++;
				Thread.sleep(1000);
//				System.out.println("task2 i : " + AtomicLongTest.i);
				System.out.println("task2 acl : " + AtomicLongTest.acl.get());
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
