package thread.consumerandproducer;

/**
 * 生产者和消费者，1.5之前可以通过 wait 和 notifyall实现
 * @author threenoodles
 *
 */
public class ConsumerAndProducer {

	public static void main(String[] args) {
		
		 Godown godown = new Godown(30,100); 
         Consumer c1 = new Consumer(15, godown); 
         Producer p1 = new Producer(10, godown); 

         c1.start(); 
         p1.start(); 
	}
}

class Godown{
	private int remain = 30;
	private int maxRemain = 100;
	public Godown(int remain,int maxRemain){
		this.remain = remain;
		this.maxRemain = maxRemain;
	}
	
	public synchronized void produce(int count) throws InterruptedException{
		
		while(remain + count > maxRemain){
			wait();
		}
		remain += count;
		System.out.println("已经生产了" + count + "个产品，现仓储量为" + remain); 
        //唤醒在此对象监视器上等待的所有线程 
        notifyAll(); 
	}
	
	public synchronized void consume(int count) throws InterruptedException{
		
		while(remain < count){
			wait();
		}
		remain -= count;
		System.out.println("已经消费了" + count + "个产品，现仓储量为" + remain); 
		notifyAll();
	}
}

//生产者
class Producer extends Thread{
	private int count;
	private Godown gd; 
	public Producer(int count,Godown gd){
		this.count = count;
		this.gd = gd;
	}
	
	@Override
	public void run() {
		try {
			while (true) {
				gd.produce(this.count);
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
//消费者
class Consumer extends Thread{
	private int count;
	private Godown gd; 
	public Consumer(int count,Godown gd){
		this.count = count;
		this.gd = gd;
	}
	
	@Override
	public void run() {
		try {
			while (true) {
				gd.consume(this.count);
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}