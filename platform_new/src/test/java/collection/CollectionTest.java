package collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CollectionTest {

	public static void main(String[] args) {

		List<Integer> lst = new ArrayList<Integer>(10);
		lst.add(1);
		lst.add(2);
		lst.add(3);
		lst.add(4);
		lst.add(5);
		lst.add(6);
		lst.add(7);

//		removeTest(lst);
		//System.out.println(lst.size());

		List<Text> testList = new ArrayList<Text>(10);
		Text text = null;
		for(int i =0;i<=9;i++){
			text = new Text();
			text.setId(String.valueOf(i));
			text.setName("name : " + i);
			testList.add(text);
		}
		System.out.println(testList.get(5).toString());
		
		Map<String,String> map = new HashMap<String,String>();
	}

	public static void removeTest(List<Integer> lst) {

		for (Integer integer : lst) {
			if (integer % 2 == 0) {
				lst.remove(integer);
			}
		}
	}

	public static void removeTest1(List<Integer> lst) {

		Iterator<Integer> itr = lst.iterator();
		while (itr.hasNext()) {
			if (itr.next() % 2 == 0) {
				itr.remove();
			}
		}
	}
}

class Text {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "id : " + this.id  +"; name : "+  this.name;
	}
}
