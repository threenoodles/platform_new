package collection;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


public class ConcurrentHashMapTest {
	
	public static void main(String[] args) {
		
		ConcurrentHashMap<String,String> map = new ConcurrentHashMap<String,String>();
		
		Set<String> pushTokens = new HashSet<String>();
		pushTokens.add("1");
		pushTokens.add("2");
		pushTokens.add("3");
		pushTokens.add("4");
		pushTokens.add("1");
		
		System.out.println(pushTokens.toString());
	}
}
