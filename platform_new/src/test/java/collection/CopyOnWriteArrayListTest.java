package collection;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class CopyOnWriteArrayListTest {

	public static void main(String[] args) {

		//JAVA中两个不同的引用指向同一个对象，当第一个引用指向另外一个对象时，第二个引用还将保持原来的对象。
		//CopyOnWriteArrayList 对于写操作是新建一个新的数组副本来完成的，保证了迭代中可以修改数组内容的特性不会抛出：ConcurrentModificationException
		//应用场景一般是读多写少得缓存中
		List<String> cpl = new CopyOnWriteArrayList<String>();
		cpl.add("Test1");
		cpl.add("Test2");
		cpl.add("Test3");
		cpl.add("Test4");
		cpl.add("Test5");
		
		for(String str : cpl){
			
			cpl.set(1, "Lest");
		}
		
		System.out.println(cpl.get(1));
	}
}
