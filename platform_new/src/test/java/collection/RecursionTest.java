package collection;


public class RecursionTest {
	
	
	public static void recursion(int time){
		System.out.println("====>" + time);
		if(++time <2){
			recursion(time);
			recursion(time);
		}
		
		System.out.println("---->" + time);
	}
	public static void main(String[] args) {
		
		recursion(0);
		System.out.println("---->ok");
	}
}
