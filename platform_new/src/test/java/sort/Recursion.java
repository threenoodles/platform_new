package sort;


public class Recursion{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		System.out.println(Recursion.recursion(3));
		Recursion.recursion(3);
	}

	public static void recursion(int num) {
		if (num <= 0) {
			System.out.println("===>" + num );
			return;
		}

		recursion(num - 1);
		recursion(num - 1);
		
		
		// 注意这样和return中调用是不一样的过程，return的时候不会产生压栈操作，不是递归调用，虽然结果是一样的
		System.out.println("--->" + num );

		
//		return resu;
		// System.out.println("--->" + num);
		// return (recursion(num - 1) * num);
	}
}
