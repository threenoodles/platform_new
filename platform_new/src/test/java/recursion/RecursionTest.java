package recursion;

import net.sf.json.util.JSONUtils;

public class RecursionTest {

	public static void recursion(int time) {
//		if (time > 0) {
//			System.out.println("这是第 " + (time --) + "次调用！");
//			recursion(time);
//		}
//		System.out.println("===>" + time);
		System.out.println("===>" + time);
		if(time <2){
			time ++;
			recursion(time);
			recursion(time);
			System.out.println("---->");
		}
		System.out.println(time);
	}

	public static void main(String[] args) {
//		recursion(1);
		
		ObjTest obj = new ObjTest();
		obj.setId(1);
		obj.setName("name");
		
		
	}
}

class ObjTest{
	private Integer id;
	private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
