<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html manifest="/jsonp.appcache">
<!-- <html> -->
<head>

<title>JSONP</title>
<meta charset="utf-8">
<meta http-equiv="cleartype" content="on">
<meta name="description" content="JSONP，专注互联网技术文章分享">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="640">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
<meta name="format-detection" content="telephone=no, email=no">
<meta name="msapplication-TileColor" content="#3199e8">
<meta name="mobile-web-app-capable" content="yes">

<link rel="stylesheet" type="text/css"
	href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

<script type="text/javascript"
	src="/assets/js/js_lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript"
	src="/assets/js/js_lib/jquery.json.js"></script>
<script type="text/javascript" src="/assets/js/js_lib/bootstrap.min.js"></script>
<style type="text/css">

/************************************************************************************
STRUCTURE
*************************************************************************************/
.dropdown-menu>.active>a,.dropdown-menu>.active>a:hover,.dropdown-menu>.active>a:focus
	{
	background-color: #1ABC9C
}

#pageView {
	width: 90%;
	height: auto;
	padding: 4em 0 1em 0;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
}

#articleView {
	width: 90%;
	height: auto;
	padding: 4em 0 1em 0;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
	display: none;
}

.j-sum-item {
	width: 100%;
	height: auto;
	background: #fff;
	margin: 1em 0 0px 0;
	padding: 0.5em;
	font-size: 1em;
	border: 1px solid #dddddd;
	border-radius: 5px;
	box-shadow: 0 1px 5px #dddddd;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-box-shadow: 0 1px 5px #dddddd;
	-moz-box-shadow: 0 1px 5px #dddddd;
	-webkit-border-radius: 5px;
}

.j-sum-item .j-sum-item-title {
	text-align: center;
	margin: 0.2em 0 0 0;
	line-height: 1.5em;
	font-size: 1.8emem;
	overflow: hidden
}

.j-sum-item .j-sum-item-time {
	text-align: center;
	margin: 1em 0 0.5em 0;
}

.j-sum-item .j-sum-item-content {
	text-align: left;
	font-size: 1.1em
}
</style>

<script type="text/javascript">
	$(function() {
		
		articleObj.loadArticles();
		//android.showToast(android.getArticles());
	});

	var articleObj = {
			
		goBack : function(){
			$("#pageView").show();
			$("#articleView").hide();
		},
		loadArticles : function(type_lang, type_cate, start, limit) {

			if(_android.network()){
				$.ajax({
					url : "/article/page",
					data : {
						type_lang : type_lang,
						type_cate : type_cate,
						start : start,
						limit : limit
					},
					type : "POST",
					dataType : "json",
					success : function(response) {

						var articles = [];
						if (response && response.articleList && response.articleList.length > 0) {

							$.each(response.articleList, function(index, data) {

								articles.push(articleObj.getArticleHtml(data));
							});
						}
						$("#pageView").html(articles.join(""));

					},
					error : function() {

					}
				});
			}else {
				_android.showToast("网络不给力，已经为您加载离线文章!");
				var articleIds = _android.getArticleIds();
				var _articleIds = $.parseJSON(articleIds);
				var _articles = [];
				var _articlesHTML = [];
				$("#pageView").html(_articleIds);
				$.each(_articleIds,function(index,data){
					
					var _article = null;
					try{
						var detail = _android.getArticle(data);
						//需要两次解析
						_article = $.parseJSON(detail);
						_article =$.parseJSON(_article.detail);
						
						_articles.push(_article);
					}catch(err){
						_article = null;
					}
					
				});
				
				$.each(_articles, function(index, data) {

					_articlesHTML.push(articleObj.getArticleHtml(data));
				});
				
				$("#pageView").html(_articlesHTML.join(""));
			}
		},
		loadArticleDetail : function(aid) {
			var _article = null;
			try{
				var detail = _android.getArticle(aid);
				//需要两次解析
				_article = $.parseJSON(detail);
				_article =$.parseJSON(_article.detail);
			}catch(err){
				_article = null;
			}
			if(!_article){
				$.ajax({
					url : "/article/" + aid,
					type : "POST",
					dataType : "json",
					success : function(response,code,result) {
						
						$("#pageView").hide();
						$("#articleView").show();
						$("#articleView").html(articleObj.getArticleDetailHtml(response.article));
						_android.updateArticle(aid,$.toJSON(response.article));
					},
					error : function() {
					}
				});				
			}else {
				$("#pageView").hide();
				$("#articleView").show();
				$("#articleView").html(articleObj.getArticleDetailHtml(_article));
			}
		},
		getArticleHtml : function(article) {
			var html = [];
			html.push('<section class="j-sum-item" data_id="');
			html.push(article.id);
			html.push('">');
			html.push('<h1 class="j-sum-item-title">');
			html
					.push('<a class="colorblack" href="#" onclick="articleObj.loadArticleDetail(');
			html.push(article.id);
			html.push(')">');
			html.push(article.title);
			html.push('</a>');
			html.push('</h1>');
			html.push('<h4 class="j-sum-item-time colormgray">');
			html.push(article.time);
			html.push('</h4>');
			html.push('<p class="j-sum-item-content colordgray">');
			html.push(article.summary);
			html.push('</p></section>');
			return html.join("");
		},
		getArticleDetailHtml : function(article) {
			var html = [];
			html.push('<section class="j-sum-item">');
			html.push('<ul id="j_back" class="pager">');
			html.push('<li class="previous"><a href="#" onclick="articleObj.goBack()">←Back</a></li>');
			html.push('</ul>');
			html.push('<h1 class="j-sum-item-title">');
			html.push('<span class="colorblack">');
			html.push(article.title);
			html.push('</span>');
			html.push('</h1>');
			html.push('<h5 class="j-sum-item-time colormgray">');
			html.push(article.time);
			html.push('</h5>');
			html.push('<p class="j-sum-item-content colordgray">');
			html.push(article.content);
			html.push('</p>');
			html.push('</section>');

			return html.join("");
		}
	}
</script>
</head>

<body>

	<div class="navbar navbar-default navbar-fixed-top j-nav">
		<div class="btn-group j-nav-more">
			<button type="button"
				class="btn btn-success  dropdown-toggle colorwhite j-btn"
				data-toggle="dropdown">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<ul class="dropdown-menu" style="width: 100%">
				<c:forEach items="${cateList}" var="cate">
					<c:choose>
						<c:when test="${cate.id == typeCate}">
							<li class="active"><a
								href="/article/list?type_lang=${typeLang}&type_cate=${cate.id}&start=${start}&limit=${limit}">${cate.name}</a></li>
						</c:when>
						<c:otherwise>
							<li><a
								href="/article/list?type_lang=${typeLang}&type_cate=${cate.id}&start=${start}&limit=${limit}">${cate.name}</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ul>
		</div>

		<div class="btn-group j-nav-selects">
			<button type="button"
				class="btn btn-success  dropdown-toggle colorwhite j-btn"
				data-toggle="dropdown">
				<c:forEach items="${langList}" var="lang">
					<c:choose>
						<c:when test="${lang.id == typeLang}">
							${lang.name}
						</c:when>
					</c:choose>
				</c:forEach>

				<span class="caret"> </span>
			</button>
			<ul class="dropdown-menu" style="width: 100%">
				<c:forEach items="${langList}" var="lang">
					<c:choose>
						<c:when test="${lang.id == typeLang}">
							<li class="active"><a
								href="/article/list?type_lang=${lang.id}&type_cate=${typeCate}&start=${start}&limit=${limit}">${lang.name}</a></li>
						</c:when>
						<c:otherwise>
							<li><a
								href="/article/list?type_lang=${lang.id}&type_cate=${typeCate}&start=${start}&limit=${limit}">${lang.name}</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ul>

		</div>

		<div class="btn-group j-nav-search">
			<input type="text" class="form-control js-content"
				placeholder="Search">
		</div>

	</div>


	<div id="pageView">

		<%-- <c:forEach items="${articleList}" var="article">
			<section class="j-sum-item">
				<h1 class="j-sum-item-title">
					<a class="colorblack" href="/article/${article.id}">${article.title}</a>
				</h1>

				<h4 class="j-sum-item-time colormgray">
					<fmt:formatDate value="${article.time}" pattern="yyyy-MM-dd" />
				</h4>
				<p class="j-sum-item-content colordgray">${article.summary}</p>

			</section>
		</c:forEach> --%>

	</div>

	<div id="articleView">
		
	</div>

</body>
</html>