<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<title>免费午餐</title>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=yes, target-densitydpi=device-dpi" />

<link rel="stylesheet" type="text/css"
	href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

<style type="text/css">

/************************************************************************************
STRUCTURE
*************************************************************************************/
body {
	min-width: 20em;
	min-height: 100%;
	width: 100%;
	background: #fefefe;
	color: #333;
	font: 100%/1.5 "Helvetica Neue", "Microsoft Yahei", sans-serif;
	padding-bottom: 10px;
	　　
}

#pageView {
	width: 80%;
	height: auto;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	/* background-color: #f2f2f2 */
}

.j-bask-item {
	width: 100%;
	height: auto;
	min-height: 250px;
	*height: 50px;
	float: left;
	background: #fff;
	margin: 15px 0 20px 0;
	padding: 10px 10px 5px 15px;
	border-radius: 5px;
	overflow: hidden;
	position: relative; /* background-color: #f6f6f6 */
	text-align: center
}

.j-bask-item img {
	
}
</style>

</head>

<body>
	<div id="pageView">


		<section class="j-bask-item">
			<ul class="pager">
				<li class="previous"><a href="/promotion/list">←back</a></li>
			</ul>

			<img alt="" src="/assets/images/zero.png">
		</section>

	</div>

</body>
</html>