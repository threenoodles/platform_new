<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf8" />
<title>Table</title>
<script type="text/javascript"
	src="/assets/js/js_lib/jquery-1.11.1.min.js"></script>
<style type="text/css">
* {
	box-sizing: border-box
}

.m-table-container {
	width: 90%;
	margin: 0 auto;
	position: relative;
}

.m-table-container .m-timer {
	width: 200px;
	height: 30px;
	margin: 0px 0 5px 0;
	padding: 0px 0 0 5px;
	float: left;
	display: block;
	border-radius: 3px;
	border: 1px solid #ddd;
}

.m-table-container .m-seacher {
	width: 200px;
	height: 30px;
	margin: 0px 30px 5px 0;
	padding: 0px 0 0 5px;
	display: block;
	border-radius: 3px;
	border: 1px solid #ddd;
	float: right;
}

.m-table {
	width: 100%;
	font-size: 16px;
	border-left: 0px;
	border: 1px solid #ddd;
	border-radius: 5px;
}

.m-table tr:hover {
	background-color: #f1f1f1
}

.m-table .m-active {
	background-color: #f7f7f7
}

.m-table thead td {
	font-size: 17px;
	font-weight: bolder;
	border-top: 0px;
}

.m-table td {
	padding: 10px;
	border: 0px;
	font-size: 16px;
	border-top: 1px solid #ddd
}

.m-table-container .selector {
	width: 100px;
	font-size: 18px;
	min-height: 40px;
}

.m-table-container .selector label {
	font-size: 15px;
	color: gray;
}

.m-table-container .m-footer-text {
	width: 50%;
	margin: 20px 0 0 0;
	font-size: 14px;
	box-sizing: border-box;
	float: left;
}

.m-table-container .m-footer-pager {
	width: auto;
	margin: 20px 0 0 0;
	font-size: 14px;
	float: right;
}

li {
	margin: 0;
	padding: 0;
	border: 0;
	font-weight: normal;
	font-style: normal;
	font-size: 100%;
	line-height: 1;
	font-family: inherit;
}

ul {
	list-style: disc;
}

.m-footer-pager {
	border: 1px solid #ddd
}

.m-footer-pager ul {
	padding: 0px;
	margin: 0px;
}

.m-footer-pager li {
	display: inline;
	float: left;
}

.m-footer-pager li a {
	float: left;
	padding: 0 14px;
	line-height: 32px;
	border-right: 1px solid;
	border-right-color: #ddd;
	text-decoration: none
}

.m-loading {
	width: 50px;
	height: 50px;
	position: absolute;
	z-index: 10;
	top: 50%;
	left: 50%;
	margin-left: -50px;
	display: none;
}
</style>

<script>
	$(function() {

		$(".m-table").css("opacity", "0.5");
		$(".m-loading").show();
	});
</script>
</head>

<body>

	<div class="m-table-container">

		<img alt="" src="/assets/images/ajax-loader.gif" class="m-loading"> <select
			class="selector">
			<option value="10">10</option>
			<option value="20" selected="selected">20</option>
			<option value="30">30</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select> <label class="label">条每页</label> <input value="" placeholder="请输入内容"
			class="m-seacher">

		<table class="m-table" cellspacing="0">
			<thead>
				<tr>
					<td>标题1标题1</td>
					<td>标题2标题2</td>
					<td>标题3标题3</td>
					<td>标题4标题4</td>
				</tr>
			</thead>
			<tbody>
				<tr class="m-active">
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
				</tr>
				<tr>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
				</tr>
				<tr class="m-active">
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
				</tr>
				<tr>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
				</tr>

				<tr class="m-active">
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
				</tr>

				<tr>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
					<td>内容内容内容内容</td>
				</tr>
			</tbody>
		</table>

		<div class="m-footer-text">展示从 1 到 20 条数据，共 661 条数据</div>
		<div class="m-footer-pager">
			<ul>
				<li><a href="#">← 上一页</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">下一页 → </a></li>
			</ul>
		</div>
	</div>
</body>
</html>
