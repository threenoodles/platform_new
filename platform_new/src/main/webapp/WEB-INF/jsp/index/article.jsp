<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi" />

<link rel="stylesheet" type="text/css"
	href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

<script type="text/javascript"
	src="/assets/js/js_lib/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/assets/js/js_lib/bootstrap.min.js"></script>
<style type="text/css">

/************************************************************************************
STRUCTURE
*************************************************************************************/

#pageView {
	width: 90%;
	height: auto;
	padding: 4em 0 1em 0;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
}

.j-sum-item {
	width: 100%;
	height: auto;
	background: #fff;
	margin: 1em 0 0 0;
	padding: 0.5em;
	font-size: 1em;
	border: 1px solid #dddddd;
	border-radius: 5px;
	border-radius: 5px;
	box-shadow: 0 1px 5px #dddddd;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-box-shadow: 0 1px 5px #dddddd;
	-moz-box-shadow: 0 1px 5px #dddddd;
	-webkit-border-radius: 5px;
}

.j-sum-item .j-sum-item-title {
	text-align: center;
	margin: 0.2em 0 0 0;
	line-height: 1.8em;
	font-size: 2em;
	overflow: hidden
}

.j-sum-item .j-sum-item-time {
	text-align: center;
	margin: 1em 0 0.5em 0;
}

.j-sum-item .j-sum-item-content {
	text-align: left;
	font-size: 1.2em;
	line-height: 1.4em; 
}


</style>

</head>

<body>

	<nav class="navbar navbar-default navbar-fixed-top j-nav">
		<div class="btn-group j-nav-more">
			<button type="button"
				class="btn btn-success  dropdown-toggle colorwhite j-btn"
				data-toggle="dropdown">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<ul class="dropdown-menu">
				<li><a href="#">最新动态</a></li>
				<li><a href="#">面试题</a></li>
			</ul>
		</div>

		<div class="btn-group j-nav-selects">
			<button type="button"
				class="btn btn-success  dropdown-toggle colorwhite j-btn"
				data-toggle="dropdown">
				JAVA<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" style="width: 100%">
				<li><a href="#">JAVA</a></li>
				<li><a href="#">PYTHON</a></li>
				<li><a href="#">C</a></li>
				<li><a href="#">C++</a></li>
			</ul>
		</div>

		<div class="btn-group j-nav-search">
			<input type="text" class="form-control js-content"
				placeholder="Search">
		</div>

	</nav>


	<div id="pageView">

		<section class="j-sum-item">
			<ul id="j_back" class="pager">
				<li class="previous"><a href="/article/list">←Back</a></li>  
			</ul>

			<h1 class="j-sum-item-title">
				<a class="colorblack" href="/article/${article.id}">${article.title}3</a>
			</h1>

			<h5 class="j-sum-item-time colormgray">
				<fmt:formatDate value="${article.time}" pattern="yyyy-MM-dd" />
			</h5>
			<p class="j-sum-item-content colordgray">${article.content}</p>

		</section>

	</div>

</body>
</html>