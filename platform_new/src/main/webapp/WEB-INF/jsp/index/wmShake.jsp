<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<!-- <html> -->
<head>

<title>JSONP</title>
<meta charset="utf-8">
<meta http-equiv="cleartype" content="on">
<meta name="description" content="JSONP，专注互联网技术文章分享">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="640">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
<meta name="format-detection" content="telephone=no, email=no">
<meta name="msapplication-TileColor" content="#3199e8">
<meta name="mobile-web-app-capable" content="yes">

<link rel="stylesheet" type="text/css"
	href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

<script type="text/javascript"
	src="/assets/js/js_lib/jquery-1.11.1.min.js"></script>
<style type="text/css">

/************************************************************************************
STRUCTURE
*************************************************************************************/
.dropdown-menu>.active>a,.dropdown-menu>.active>a:hover,.dropdown-menu>.active>a:focus
	{
	background-color: #2BB6AA
}

html,body {
	height: 100%;
	width: 100%;
	font-size: 100%;
	overflow: hidden;
}

#pageHeader {
	height: 2.5em;
	border-bottom: 1px solid #ddd;
	background-color: #2BB6AA;
	text-align: center;
	position: relative;
	padding: 0.8em;
	font-size: 1.8em;
	color: #ffffff;
	box-sizing: border-box;
}

#pageView {
	width: 100%;
	height: 100%;
	padding: 0em 0 1em 0;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
	overflow: hidden;
}

#pageView .shake {
	width: 100%;
	height: 100%;
	font-size: 100%;
}
</style>

<script type="text/javascript">
	$(function() {
		if (_android) {
			_android.setLoadStatus(100);
		}
		window.setInterval(function(){
			_android.showToast(_android.getLoadStatus());
		},1000);
	});
</script>
</head>

<body>

	<header id="pageHeader" class="f-header">
		<span class="clr-white"> 望京国际研发园 </span>
	</header>
	<div id="pageView">
		<img class="shake" alt="shake" src="/assets/images/shake.jpeg">
	</div>

</body>
</html>