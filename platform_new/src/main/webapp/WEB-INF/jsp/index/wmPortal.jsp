<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<!-- <html> -->
<head>

<title>度度饿了美</title>
<meta charset="utf-8">
<meta http-equiv="cleartype" content="on">
<meta name="description" content="JSONP，专注互联网技术文章分享">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="640">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
<meta name="format-detection" content="telephone=no, email=no">
<meta name="msapplication-TileColor" content="#3199e8">
<meta name="mobile-web-app-capable" content="yes">

<link rel="stylesheet" type="text/css"
	href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

<script type="text/javascript"
	src="/assets/js/js_lib/jquery-1.11.1.min.js"></script>
<style type="text/css">

/************************************************************************************
STRUCTURE
*************************************************************************************/
.dropdown-menu>.active>a,.dropdown-menu>.active>a:hover,.dropdown-menu>.active>a:focus
	{
	background-color: #1ABC9C
}


html,body {
	font-size: 100%;
	overflow: hidden;
	width: 100%;
	height: 100%;
}

#pageView {
	width: 100%;
	height: 100%;
	padding: 1em 0 1em 0;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
	background-color: #2BB6AA
}

.cover-heading {
	color: #fff;
	text-align: center;
	font-size: 3em;
}

.lead{
	margin-bottom: 20px;
	font-size: 16px;
	font-weight: 300;
	line-height: 1.4;
	text-align: center;
	margin: 2em;
}

#pageView .shake {
	width: 100%;
	height: 100%;
}
</style>


<script type="text/javascript">
	$(function() {
		if(_android){
			_android.showToast("working ...");
		}
		
		$("#shake").click(function(){
			_android.showToast("going ...");
			_android.startShake();
		});
	});
</script>

</head>

<body>
	<div id="pageView">
		<h1 class="cover-heading">度度饿了美</h1>
		<p class="lead">
			<a href="#" class="btn btn-lg btn-default">省的你害怕</a>
			<br />
			<br />
			<a id="shake" href="#" class="btn btn-lg btn-default">美食摇一摇</a>
			<br />
			<br />
			<a href="#" class="btn btn-lg btn-default">比的你害怕</a>
		</p>

	</div>
</body>
</html>