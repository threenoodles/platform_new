<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<!-- <html> -->
<head>

<title>JSONP</title>
<meta charset="utf-8">
<meta http-equiv="cleartype" content="on">
<meta name="description" content="JSONP，专注互联网技术文章分享">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="640">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
<meta name="format-detection" content="telephone=no, email=no">
<meta name="msapplication-TileColor" content="#3199e8">
<meta name="mobile-web-app-capable" content="yes">

<link rel="stylesheet" type="text/css"
	href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/common.css" />

<script type="text/javascript"
	src="/assets/js/js_lib/jquery-1.11.1.min.js"></script>
<style type="text/css">

/************************************************************************************
STRUCTURE
*************************************************************************************/
.dropdown-menu>.active>a,.dropdown-menu>.active>a:hover,.dropdown-menu>.active>a:focus
	{
	background-color: #1ABC9C
}

.float-left {
	float: left;
}

.float-right {
	float: right;
}

.clr-orange {
	color: #ff9900;
}

.clr-white {
	color: #ffffff;
}

.clr-green {
	color: #2BB6AA;
}

body{
	font-size: 100%;
}

#pageHeader {
	height: 2.5em;
	border-bottom: 1px solid #ddd;
	background-color: #2BB6AA;
	text-align: center;
	position: relative;
	padding: 0.8em;
	font-size: 1.8em;
	color: #ffffff;
	box-sizing : border-box;  
}

}
#pageView {
	width: 90%;
	height: auto;
	padding: 1em 0 1em 0;
	margin: 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
}

#articleView {
	width: 90%;
	height: auto;
	padding: 1em 0 1em 0; margin : 0 auto;
	position: relative;
	margin: 0 auto;
	font-size: 100%;
	margin: 0 auto;
	display: none;
	margin: 0 auto;
}

.f-sum-item {
	width: 100%;
	height: 13em;
	background: #fff;
	margin: 1em 0% 0 0;
	padding: 0.5em;
	font-size: 1em;
	border: 1px solid #dddddd;
	border-radius: 5px;
	box-shadow: 0 1px 5px #dddddd;
	box-sizing: border-box;
	position: relative;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-box-shadow: 0 1px 5px #dddddd;
	-moz-box-shadow: 0 1px 5px #dddddd;
	-webkit-border-radius: 5px;
}

.f-sum-item .f-img {
	width: 30%;
	height: 10em;
	position: relative;
	float: left;
	margin: 1.5em 0 0 0;
}

.f-sum-item .f-rest-name {
	width: 100%;
	padding: 0 0 0 0.5em;
	position: relative;
	text-align: left;
	color: #2BB6AA; 
	font-size: 1.7em;
	text-align: center;
}

.f-sum-item .f-rest-intro {
	float: right;
	width: 70%;
	padding: 0.1em 0 0 0.5em;
	position: relative;
	text-align: left;
	color: #999999;
	font-size: 1.3em
}

.f-sum-item .f-rest-type-new {
	position: absolute;
	background: url(/assets/images/all.png) no-repeat -289px 0px;
	top: 0%;
	left: 100%;
	z-index: 2;
	width: 45px;
	height: 45px;
	margin-left: -45px;
}

.f-sum-item .f-rest-type-brand {
	position: absolute;
	background: url(/assets/images/all.png) no-repeat -335px 0px;
	top: 0%;
	left: 100%;
	z-index: 2;
	width: 45px;
	height: 45px;
	margin-left: -45px;
}

.f-sum-item .f-rest-type-hot {
	position: absolute;
	background: url(/assets/images/all.png) no-repeat -244px 0px;
	top: 0%;
	left: 100%;
	z-index: 2;
	width: 45px;
	height: 45px;
	margin-left: -45px;
}

/**==================more======================**/
.j-sum-more {
	position: relative;
	width: 100%;
	left: 0;
	bottom: 0;
	height: 60px;
	text-align: center;
	clear: both;
	margin: 1em 0 0 0
}

.j-sum-more h3 {
	margin: 10px 0;
	text-align: center;
	clear: both;
}
</style>

</head>

<body>

	<header id="pageHeader" class="f-header">
		<span class="clr-white"> 望京国际研发园 </span> 
	</header>
	<div id="pageView">
		<section class="f-sum-item float-left">

			<img class="f-img" alt=""
				src="/assets/images/foods/200708-2007080311581968750.jpg">

			<div class="f-rest-name">肯德基外卖</div>
			<div class="f-rest-intro">评价：★★★★★</div>
			<div class="f-rest-intro">起送：20元</div>
			<div class="f-rest-intro">优惠：满20减12（赠可乐）</div>
			<div class="f-rest-intro">速度：45分钟</div>
			<div class="f-rest-intro">来自：美团外卖</div>
			<div class="f-rest-type-brand"></div>

		</section>

		<section class="f-sum-item float-right">

			<img class="f-img" alt=""
				src="/assets/images/foods/2006723112535166.jpg">
			<div class="f-rest-name">俏江南</div>
			<div class="f-rest-intro">评价：★★★★★</div>
			<div class="f-rest-intro">起送：20元</div>
			<div class="f-rest-intro">优惠：满20减12（赠可乐）</div>
			<div class="f-rest-intro">速度：45分钟</div>
			<div class="f-rest-intro">来自：饿了么</div>
			<div class="f-rest-type-new"></div>
		</section>

		<section class="f-sum-item float-left">

			<img class="f-img" alt=""
				src="/assets/images/foods/2014111810470460.jpg">
			<div class="f-rest-name">必胜客</div>
			<div class="f-rest-intro">评价：★★★★★</div>
			<div class="f-rest-intro">起送：20元</div>
			<div class="f-rest-intro">优惠：满20减12（赠可乐）</div>
			<div class="f-rest-intro">速度：45分钟</div>
			<div class="f-rest-intro">来自：饿了么</div>
			<div class="f-rest-type-hot"></div>
		</section>

		<section class="f-sum-item float-right">

			<img class="f-img" alt=""
				src="/assets/images/foods/2014111810470460.jpg">
			<div class="f-rest-name">汉拿山</div>
			<div class="f-rest-intro">评价：★★★★★</div>
			<div class="f-rest-intro">起送：20元</div>
			<div class="f-rest-intro">优惠：满20减12（赠可乐）</div>
			<div class="f-rest-intro">速度：45分钟</div>
			<div class="f-rest-intro">来自：美餐网</div>
		</section>

		<section class="f-sum-item float-left">

			<img class="f-img" alt=""
				src="/assets/images/foods/2014111810470460.jpg">
			<div class="f-rest-name">肯德基外卖</div>
			<div class="f-rest-intro">评价：★★★★★</div>
			<div class="f-rest-intro">起送：20元</div>
			<div class="f-rest-intro">优惠：满20减12（赠可乐）</div>
			<div class="f-rest-intro">速度：45分钟</div>
			<div class="f-rest-intro">来自：美餐网</div>
		</section>

		<section class="f-sum-item float-right">

			<img class="f-img" alt=""
				src="/assets/images/foods/2014111810470460.jpg">
			<div class="f-rest-name">肯德基外卖</div>
			<div class="f-rest-intro">评价：★★★★★</div>
			<div class="f-rest-intro">起送：20元</div>
			<div class="f-rest-intro">优惠：满20减12（赠可乐）</div>
			<div class="f-rest-intro">速度：45分钟</div>
		</section>

		<section class="f-sum-item j-sum-more float-left">
			<h3 class="f-sum-item-time colordgray">
				<a href="/promotion/basklist">查看更多</a>
			</h3>
		</section>

		<!-- 手机号 -->
		<!-- <div class="col-lg-12" style="padding: 0px;margin-top: 30px">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="输入手机号，看您能省多少2w"> 
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">走起</button>
				</span>
			</div>
		</div> -->

	</div>

</body>
</html>