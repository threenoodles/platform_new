<%@page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<title>Netty Example</title>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="/assets/js/push_client.js" charset="UTF-8"></script>
</head>
<body>
    <p>心跳状态：<span id="keeplive_box">检测中...</span></p>

    <div id="container">
    </div>
    <input id="txt" type="text">
    <button id="btn">send</button>

     <%=request.getAttribute("wpush_server_url")%>;
    <script>
        function changeStatus(msg){
            var keeplive_box = document.getElementById('keeplive_box');
            keeplive_box.innerHTML="<span style='color:red;'>"+msg+"</span>";
        };
        var webSocket = new DevWebSocketClient("waimai_e",13344);
        webSocket.onMessageListener(function(msg){
            addMessage(msg);
        });
        webSocket.onOpenListener(function(event){
            changeStatus("连接服务socket成功");
        });
        webSocket.onCloseListener(function(event){
            changeStatus("服务socket已关闭");
        });
        webSocket.onErrorListener(function(event){
            changeStatus("服务socket出错");
        });

        var btn = document.getElementById('btn');
        var txt = document.getElementById('txt');
        btn.addEventListener('click',function (){
            webSocket.send(txt.value)
        }, false);

        var addMessage = function(msg) {
            var container = document.getElementById('container');
            var txtElem = document.createElement('p');
            var txt = document.createTextNode(msg);

            txtElem.appendChild(txt);
            container.appendChild(txtElem);
        };

    </script>
</body>
</html>