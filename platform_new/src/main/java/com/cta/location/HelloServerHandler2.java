package com.cta.location;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.ssl.SslHandler;

import com.cta.location.handler.MyServerHandler;

public class HelloServerHandler2 extends MyServerHandler{

	private WebSocketServerHandshaker handshaker = null;
	private final String websocketPath = "/websocket";
	private ChannelPipeline cp = null;
	private int count = 0;
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		Channel ch = ctx.channel();
		ch.write(msg + " HelloServerHandler2 \r\n");
		System.out.println(msg + " HelloServerHandler2 channelRead");
//		ctx.fireChannelRead(msg);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);

		System.out.println("HelloServerHandler2 : " + ctx.channel().remoteAddress()
				+ " active !");
	}
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		
		System.out.println("HelloServerHandler2 channelReadComplete");
		ctx.flush();
		ctx.fireChannelReadComplete();
	};

	 private String getWebSocketLocation(ChannelPipeline cp, HttpRequest req) {
	        String protocol = "ws";
	        if (cp.get(SslHandler.class) != null) {
	            protocol = "wss";
	        }
	        return protocol + "://" + req.headers().get(HttpHeaders.Names.HOST) + websocketPath;
	    }
}
