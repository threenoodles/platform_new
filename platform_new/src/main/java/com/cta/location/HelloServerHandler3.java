package com.cta.location;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;


public class HelloServerHandler3 extends ChannelInboundHandlerAdapter{

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		Channel ch = ctx.channel();
		ch.write(msg + "\r\n");
		System.out.println(msg + " HelloServerHandler3 channelRead");
		
		ctx.fireChannelRead(msg);
	}

	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		
		System.out.println("HelloServerHandler3 channelReadComplete");
		ctx.flush();
		ctx.fireChannelReadComplete();
	};
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);

		System.out.println("HelloServerHandler3 : " + ctx.channel().remoteAddress()
				+ " active !");
	}
}
