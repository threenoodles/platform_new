package com.cta.location;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

public class HelloServerHandler extends ChannelInboundHandlerAdapter {

	private WebSocketServerHandshaker handshaker = null;
	private final String websocketPath = "/websocket";
	private ChannelPipeline cp = null;
	private int count = 0;
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		if ((msg instanceof FullHttpRequest)) {
			FullHttpRequest req = (FullHttpRequest) msg;
			// Handshake
			WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory(
					getWebSocketLocation(ctx.pipeline(), req), null, false);
			handshaker = wsFactory.newHandshaker(req);
			if (handshaker == null) {
				WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
			} else {

				final ChannelFuture handshakeFuture = handshaker.handshake(
						ctx.channel(), req); // 这里说白了就是进行握手，向客户端返回用于建立连接的报文
				handshakeFuture
						.addListener(new GenericFutureListener<Future<? super Void>>() {
							@Override
							public void operationComplete(
									Future<? super Void> future)
									throws Exception {
								System.out.println("handle success !");
							}
						});
			}

		} else if (msg instanceof WebSocketFrame) {
			Channel ch = ctx.channel();
			ch.writeAndFlush("TEST");
		} else {
			Channel ch = ctx.channel();
			ch.write(msg + "\r\n");
			System.out.println("count is : " + ++count);
			System.out.println(msg + " HelloServerHandler channelRead");
			ctx.fireChannelRead(msg);
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);

		System.out.println("HelloServerHandler : " + ctx.channel().remoteAddress()
				+ " active !");
	}
	
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		
		System.out.println("HelloServerHandler channelReadComplete");
		ctx.flush();
		ctx.fireChannelReadComplete();
	};

	 private String getWebSocketLocation(ChannelPipeline cp, HttpRequest req) {
	        String protocol = "ws";
	        if (cp.get(SslHandler.class) != null) {
	            protocol = "wss";
	        }
	        return protocol + "://" + req.headers().get(HttpHeaders.Names.HOST) + websocketPath;
	    }
}
