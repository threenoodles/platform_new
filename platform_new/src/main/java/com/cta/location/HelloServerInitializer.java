package com.cta.location;

import java.util.LinkedList;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

public class HelloServerInitializer extends ChannelInitializer<SocketChannel>{

	/**
		Blocking operations
		As said before you MUST NOT block the IO Thread at all. 
		This means doing blocking operations within your ChannelHandler is problematic. 
		Lucky enough there is a solution for this. Netty allows to specify an EventExecutorGroup 
		when adding ChannelHandlers tot he ChannelPipeline. This EventExecutorGroup will then be used 
		to obtain an EventExecutor and this EventExecutor will execute all the methods of the ChannelHandler. 
		The EventExecutor here will use a different Thread then the one that is used for the IO and thus free up the EventLoop.
	 */
	private static final EventExecutorGroup evntExec = new DefaultEventExecutorGroup(64);
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		
		ChannelPipeline pipline = ch.pipeline();
		pipline.addLast(new LineBasedFrameDecoder(1048));
		pipline.addLast(new StringDecoder());
		pipline.addLast(new StringEncoder());
		pipline.addLast(new HelloServerHandler());
		pipline.addLast(new HelloServerHandler2());
		pipline.addLast(evntExec,new HelloServerHandler3());
	}
}
