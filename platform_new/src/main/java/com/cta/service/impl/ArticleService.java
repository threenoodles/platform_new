/**
 * @FileName: IndexService.java
 * @Package com.cta.service
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 下午04:09:38
 * @version V1.0
 */
package com.cta.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cta.dao.impl.ArticleDao;
import com.cta.entity.ArticleEntity;
import com.cta.platform.util.ListPager;

/**
 * @author chenwenpeng
 * 
 */
@Service
public class ArticleService {

	@Resource
	private ArticleDao articleDao;

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> getDetailById(Long articleId)
			throws SQLException {

		return articleDao.getDetailById(articleId);
	}

	public List<ArticleEntity> getArticleByStatus(int status)
			throws SQLException {

		return articleDao.getArticleByStatus(status);
	}

	public List<ArticleEntity> getArticleListByCateAndLang(Integer typeLang,
			Integer typeCate, ListPager pager) throws SQLException {

		return articleDao.getArticleListByCateAndLang(typeLang, typeCate, pager);
	}


	public List<Map<String, Object>> getArticlCateListByStatus(
			int statusEntabled) throws SQLException {
		
		return articleDao.getArticlCateListByStatus(statusEntabled);
	}

	public List<Map<String, Object>> getArticleLangListByStatus(
			int statusEntabled) throws SQLException {
		
		return articleDao.getArticleLangListByStatus(statusEntabled);
	}
}
