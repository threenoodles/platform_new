/**
 * @FileName: IndexService.java
 * @Package com.cta.service
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 下午04:09:38
 * @version V1.0
 */
package com.cta.service.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.cta.dao.impl.StockDao;
import com.cta.entity.StockInfoEntity;
import com.cta.entity.StockTrendEntity;
import com.cta.utils.DateTimeUtil;
import com.cta.utils.HttpClientUtils;
import com.cta.utils.JSONUtil;

/**
 * @author chenwenpeng
 * 
 */
@Service
public class StockService {

	Logger logger = Logger.getLogger(StockService.class);
	@Resource
	private StockDao stockDao;

	/**
	 * @param code 
	 * @param endTiem 
	 * @param startTime 
	 * @param string 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws URISyntaxException
	 *             id int NOT NULL PRIMARY KEY AUTO_INCREMENT comment 'ID',
	 *             volume bigint NOT NULL DEFAULT 0 comment '成交量', price_open
	 *             int NOT NULL DEFAULT 0 comment '开盘价', price_heigh int NOT
	 *             NULL DEFAULT 0 comment '最高盘价', price_low int NOT NULL DEFAULT
	 *             0 comment '最低盘价', price_close int NOT NULL DEFAULT 0 comment
	 *             '收盘价', price_change int NOT NULL DEFAULT 0 comment '涨跌额',
	 *             price_change_percent int NOT NULL DEFAULT 0 comment '涨跌额',
	 *             turnrate double NOT NULL DEFAULT 0.0 comment '换手率', ma5 int
	 *             NOT NULL DEFAULT 0 comment '最近5日平均', ma10 int NOT NULL
	 *             DEFAULT 0 comment '最近10日平均', ma20 int NOT NULL DEFAULT 0
	 *             comment '最近20日平均', ma30 int NOT NULL DEFAULT 0 comment
	 *             '最近30日平均'
	 * @throws SQLException
	 */
	public String getStockTrend(StockInfoEntity si, long startTime, long endTiem) throws SQLException {
		
		StringBuilder results = new StringBuilder();
		String url = "http://xueqiu.com/stock/forchartk/stocklist.json";
		String cookie = "xq_a_token=mBYnGtwmttrMxEZKLU8WSV;xq_r_token=7ZPhx6UcV7Bw7vJQofkRYR;";
		Map<String, String> params = new HashMap<String, String>(10);
		// ?symbol=SZ002230&period=1day&type=normal&end=1419075197887&_=1419075115939
		params.put("symbol", si.getSymbol());
		params.put("period", "1day");
		params.put("type", "normal");
		params.put("begin", String.valueOf(startTime));
		params.put("end", String.valueOf(endTiem));
		params.put("_", String.valueOf(new Date().getTime()));

		String strList = null;
		JSONArray dataList = null;
		try {
			String resultStr = HttpClientUtils.simpleGetInvokeCookie(url,
					cookie, params);
			strList = JSONUtil.parseJSONObject(resultStr)
					.getString("chartlist");
			dataList = JSONUtil.parseJSONArray(strList, true);
			String result = parseAndInserStockTrend(dataList,si.getId(),si.getSymbol());
			if (StringUtils.isNotBlank(result)) {
				results.append("解析股票代码 ：" + " 中数据失败，异常数据：" + result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			results.append("解析股票代码 ： " + "失败!");
		}
		return results.toString();
	}

	/**
	 * @param dataList
	 * @throws SQLException
	 */
	private String parseAndInserStockTrend(JSONArray dataList,int stock_id,String code) {
		if(dataList == null || dataList.size()<=0){
			return StringUtils.EMPTY;
		}
		StringBuilder result = new StringBuilder();
		int dataCount = dataList.size();
		// 一条数据
		JSONObject data = null;
		for (int i = 0; i < dataCount; i++) {
			try {
				data = dataList.getJSONObject(i);
				data.put("stock_id", stock_id);
				data.put("code", code);
				insertStockTrend(data);
			} catch (Exception e) {
				result.append("插入： " + data.toString() + " 失败 !");
				e.printStackTrace();
			}
		}
		return result.toString();
	}

	/**
	 * @param data
	 * @throws SQLException
	 */
	private void insertStockTrend(JSONObject data) throws SQLException {
		// 实体类
		StockTrendEntity stockTrend = new StockTrendEntity();
		long volume = 0;
		int stock_id = 0;
		String code = "";
		int price_open = 0;
		int price_heigh = 0;
		int price_low = 0;
		int price_close = 0;
		int price_change = 0;
		double price_change_percent = 0.0;
		double turnrate = 0.0;
		int ma5 = 0;
		int ma10 = 0;
		int ma20 = 0;
		int ma30 = 0;
		Date day = null;
		stock_id = data.getInt("stock_id");
		code = data.getString("code");
		volume = data.getLong("volume");
		price_open = ((Double) (data.getDouble("open") * 100)).intValue();
		price_heigh = ((Double) (data.getDouble("high") * 100)).intValue();
		price_low = ((Double) (data.getDouble("low") * 100)).intValue();
		price_close = ((Double) (data.getDouble("close") * 100)).intValue();
		price_change = ((Double) (data.getDouble("chg") * 100)).intValue();
		price_change_percent = data.getDouble("percent");
		turnrate = data.getDouble("turnrate");
		ma5 = ((Double) (data.getDouble("ma5") * 100)).intValue();
		ma10 = ((Double) (data.getDouble("ma10") * 100)).intValue();
		ma20 = ((Double) (data.getDouble("ma20") * 100)).intValue();
		ma30 = ((Double) (data.getDouble("ma30") * 100)).intValue();
		day = new Date(data.getString("time"));

		stockTrend.setStock_id(stock_id);
		stockTrend.setCode(code);
		stockTrend.setDay(day);
		stockTrend.setVolume(volume);
		stockTrend.setPrice_open(price_open);
		stockTrend.setPrice_heigh(price_heigh);
		stockTrend.setPrice_low(price_low);
		stockTrend.setPrice_close(price_close);
		stockTrend.setPrice_change(price_change);
		stockTrend.setPrice_change_percent(price_change_percent);
		stockTrend.setTurnrate(turnrate);
		stockTrend.setMa5(ma5);
		stockTrend.setMa10(ma10);
		stockTrend.setMa20(ma20);
		stockTrend.setMa30(ma30);
		stockTrend.setDay(day);
		stockTrend.setStatus(StockTrendEntity.STATUS_ENTABLED);

		String fDate = DateTimeUtil.formatTime(stockTrend.getDay(), "yyyy-MM-dd");
		if(stockDao.existsStockTrend(code,day)){
			
			logger.info("已存在" + stockTrend.getCode()+ " " + fDate + " !");
		}else {
			stockDao.insert(stockTrend);
			logger.info("插入 " + stockTrend.getCode() + " " + fDate + " 股票信息成功 !");
		}
	}
	/**获取股票信息
	 * @return
	 */
	public String getStockInfo() {
		StringBuilder results = new StringBuilder();
		for (int i = 1; i < 100; i++) {
			JSONArray dataList = parseStockPage(i);
			if(dataList != null){
				for(int j = 0,n=dataList.size();j<n;j++){
					String result = parseAndInsertStock(dataList.getJSONObject(j));
					results.append(result);
				}
			}else {
				results.append("解析股票列表第 " + i + " 页，出错 !");
			}
		}
		return results.toString();
	}

	/**解析并插入一条股票
	 * @param object
	 */
	private String parseAndInsertStock(JSONObject obj) {
		
		StockInfoEntity si = new StockInfoEntity();
		si.setCode(obj.getString("code"));
		si.setName(obj.getString("name"));
		si.setSymbol(obj.getString("symbol"));
		si.setStatus(StockInfoEntity.STATUS_ENTABLED);
		try {
			if(stockDao.existsStock(obj.getString("symbol"))){
				
				logger.info("已经存在 ： " + obj.getString("symbol") + "  " + obj.getString("name") + " !");
			}else {
				stockDao.insert(si);
				logger.info("插入 " + si.getName() + " " + si.getSymbol() + " 股票信息成功 !");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return "插入 " + obj.getString("name")+ "  " + obj.getString("symbol") +" 股票出错！";
		}
		return StringUtils.EMPTY;
	}

	/**解析一页股票信息
	 * @param i
	 */
	private static JSONArray parseStockPage(int i) {
		String url = "http://xueqiu.com/stock/cata/stocklist.json";
		String cookie = "xq_a_token=mBYnGtwmttrMxEZKLU8WSV;xq_r_token=7ZPhx6UcV7Bw7vJQofkRYR;";
		Map<String, String> params = new HashMap<String, String>(10);
		// ?page=1&size=30&order=desc&orderby=percent&type=11%2C12&_=1419132729211
		params.put("page", String.valueOf(i));
		params.put("size", "90");
		params.put("order", "desc");
		params.put("orderby", "percent");
		params.put("type", "11,12");
		params.put("_", String.valueOf(new Date().getTime()));
		String strList = null;
		try {
			String resultStr = HttpClientUtils.simpleGetInvokeCookie(url,
					cookie, params);
			strList = JSONUtil.parseJSONObject(resultStr).getString("stocks");
			return JSONUtil.parseJSONArray(strList, true);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @return
	 * @throws SQLException
	 */
	public List<StockInfoEntity> getStockInfoList() throws SQLException{
		
		return stockDao.getStockInfoList();
	}
	
	public static void main(String[] args) {
	
		System.out.println(parseStockPage(1));
	}

	/**
	 * @param start
	 * @param end
	 * @return
	 */
	public List<StockTrendEntity> getStockTrendByStockIdAndDayAndIP(Date start,
			Date end, int stockId, double increasePercent) {
		try{
			List<StockTrendEntity> counts = stockDao.getStockTrendByStockIdAndDayAndIP(start,end,stockId,increasePercent);
			return counts;
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e);
			return new ArrayList<StockTrendEntity>(0);
		}
	}
}
