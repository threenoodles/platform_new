/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.entity;

import java.util.Date;

import com.cta.platform.constant.StrategyType;
import com.cta.platform.persistence.annotation.Column;
import com.cta.platform.persistence.annotation.ID;
import com.cta.platform.persistence.annotation.Table;

@Table(name = "s_stock_trend")
public class StockTrendEntity extends BaseEntity {

	// id int NOT NULL PRIMARY KEY AUTO_INCREMENT comment 'ID',
	// stock_id bigint NOT NULL DEFAULT 0 comment 股票ID,
	// code bigint NOT NULL DEFAULT 0 comment '股票CODE',
	// volume bigint NOT NULL DEFAULT 0 comment '成交量',
	// price_open int NOT NULL DEFAULT 0 comment '开盘价',
	// price_heigh int NOT NULL DEFAULT 0 comment '最高盘价',
	// price_low int NOT NULL DEFAULT 0 comment '最低盘价',
	// price_close int NOT NULL DEFAULT 0 comment '收盘价',
	// price_change int NOT NULL DEFAULT 0 comment '涨跌额',
	// price_change_percent double NOT NULL DEFAULT 0.0 comment '涨跌率',
	// turnrate double NOT NULL DEFAULT 0.0 comment '换手率',
	// ma5 int NOT NULL DEFAULT 0 comment '最近5日平均',
	// ma10 int NOT NULL DEFAULT 0 comment '最近10日平均',
	// ma20 int NOT NULL DEFAULT 0 comment '最近20日平均',
	// ma30 int NOT NULL DEFAULT 0 comment '最近30日平均',
	// day DATE NOT NULL DEFAULT '0000-00-00' comment '时间'

	@ID(strategy = StrategyType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "stock_id")
	private Integer stock_id;
	@Column(name = "code")
	private String code;
	@Column(name = "volume")
	private Long volume;
	@Column(name = "price_open")
	private Integer price_open;
	@Column(name = "price_heigh")
	private Integer price_heigh;
	@Column(name = "price_low")
	private Integer price_low;
	@Column(name = "price_close")
	private Integer price_close;
	@Column(name = "price_change")
	private Integer price_change;
	@Column(name = "price_change")
	private Double price_change_percent;
	@Column(name = "turnrate")
	private Double turnrate;
	@Column(name = "ma5")
	private Integer ma5;
	@Column(name = "ma10")
	private Integer ma10;
	@Column(name = "ma20")
	private Integer ma20;
	@Column(name = "ma30")
	private Integer ma30;
	@Column(name = "day")
	private Date day;
	@Column(name = "status")
	private Integer status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public Integer getPrice_open() {
		return price_open;
	}

	public void setPrice_open(Integer price_open) {
		this.price_open = price_open;
	}

	public Integer getPrice_heigh() {
		return price_heigh;
	}

	public void setPrice_heigh(Integer price_heigh) {
		this.price_heigh = price_heigh;
	}

	public Integer getPrice_low() {
		return price_low;
	}

	public void setPrice_low(Integer price_low) {
		this.price_low = price_low;
	}

	public Integer getPrice_close() {
		return price_close;
	}

	public void setPrice_close(Integer price_close) {
		this.price_close = price_close;
	}

	public Integer getPrice_change() {
		return price_change;
	}

	public void setPrice_change(Integer price_change) {
		this.price_change = price_change;
	}

	public Double getPrice_change_percent() {
		return price_change_percent;
	}

	public void setPrice_change_percent(Double price_change_percent) {
		this.price_change_percent = price_change_percent;
	}

	public Double getTurnrate() {
		return turnrate;
	}

	public void setTurnrate(Double turnrate) {
		this.turnrate = turnrate;
	}

	public Integer getMa5() {
		return ma5;
	}

	public void setMa5(Integer ma5) {
		this.ma5 = ma5;
	}

	public Integer getMa10() {
		return ma10;
	}

	public void setMa10(Integer ma10) {
		this.ma10 = ma10;
	}

	public Integer getMa20() {
		return ma20;
	}

	public void setMa20(Integer ma20) {
		this.ma20 = ma20;
	}

	public Integer getMa30() {
		return ma30;
	}

	public void setMa30(Integer ma30) {
		this.ma30 = ma30;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStock_id() {
		return stock_id;
	}

	public void setStock_id(Integer stock_id) {
		this.stock_id = stock_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
