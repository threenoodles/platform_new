/**
 * Copyright (c) 2013 chenwenpeng
 * All rights reserved.
 * Version V1.0
 */
package com.cta.entity;

import java.util.Date;

import com.cta.platform.constant.StrategyType;
import com.cta.platform.persistence.annotation.Column;
import com.cta.platform.persistence.annotation.ID;
import com.cta.platform.persistence.annotation.Table;

@Table(name = "J_ARTICLE")
public class ArticleEntity extends BaseEntity{

	/**
	 * 要被push的文章
	 */
	public static final int STATUS_PUSH = 2;
	
	@ID(strategy = StrategyType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@Column(name = "title")
	private String title;
	@Column(name = "summary")
	private String summary;
	@Column(name = "time")
	private Date time;
	@Column(name = "status")
	private Integer status;

	@Column(name = "cate_lang")
	private Integer cate_lang;

	@Column(name = "cate_type")
	private Integer cate_type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCate_lang() {
		return cate_lang;
	}

	public void setCate_lang(Integer cate_lang) {
		this.cate_lang = cate_lang;
	}

	public Integer getCate_type() {
		return cate_type;
	}

	public void setCate_type(Integer cate_type) {
		this.cate_type = cate_type;
	}

}
