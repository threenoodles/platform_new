/**
 * 
 */
package com.cta.entity;

import java.io.Serializable;

/**
 * @author chenwenpeng
 * 
 */
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = -2729287783192433782L;

	/**
	 * 
	 */
	public static final int STATUS_UNENTABLED = 0;
	public static final int STATUS_ENTABLED = 1;
}
