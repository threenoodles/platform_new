package com.cta.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cta.entity.StockInfoEntity;
import com.cta.entity.StockTrendEntity;
import com.cta.service.impl.StockService;
import com.cta.utils.DateTimeUtil;
import com.cta.utils.NumberUtil;

@Controller
@RequestMapping("/stock")
public class StockController {

	Logger logger = Logger.getLogger(StockController.class);
	@Resource
	private StockService stockService;
	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws URISyntaxException 
	 */
	@RequestMapping("info")
	public String info(ModelMap model)
			throws IOException, SQLException, URISyntaxException {
		
		String result = stockService.getStockInfo();
		model.addAttribute("result", StringUtils.isBlank(result) ? "ok" : result);
		return "jsonView";
	}
	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws URISyntaxException 
	 */
	@RequestMapping("trend")
	public String trend(ModelMap model,@RequestParam(defaultValue="0",value="begin") long begin)
			throws IOException, SQLException, URISyntaxException {
		StringBuilder results = new StringBuilder(); 
		List<StockInfoEntity> siList = stockService.getStockInfoList();
		long startTime = 946656000000L; 
		//不传就加载最近1年的数据
		if(begin == 0){
			startTime = 1388402655197L;
		}else {
		//传了参数就根据参数加载
			startTime = begin;
		}
		long endTiem = new Date().getTime();
		
		for(StockInfoEntity si : siList){
			results.append(stockService.getStockTrend(si,startTime,endTiem));
		}
		model.addAttribute("result", StringUtils.isBlank(results.toString()) ? "ok" : results.toString());
		return "jsonView";
	}
	
	
	/**一直张婷
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 * @throws URISyntaxException
	 * @throws DateParseException 
	 */
	@RequestMapping("increasing")
	@ResponseBody
	public List<StockInfoEntity> volatility(ModelMap model
			,@RequestParam(value="ip",defaultValue="") double increasePercent
			,@RequestParam(value="start",defaultValue="") Date start
			,@RequestParam(value="end",defaultValue="") Date end)
			throws IOException, SQLException, URISyntaxException, DateParseException {
		List<StockInfoEntity> results = new ArrayList<StockInfoEntity>();
		
		List<StockInfoEntity> siList = stockService.getStockInfoList();
		List<StockTrendEntity> stList = null;
		int dayInterval = DateTimeUtil.getDayInterval(start, end);
		int count = 0;
		for(StockInfoEntity si : siList){
			stList = stockService.getStockTrendByStockIdAndDayAndIP(start,end,si.getId(),increasePercent);
			//90%的天涨停
			if(NumberUtil.div(stList.size(), dayInterval, 2) >= 0.5){
				results.add(si);
				logger.info("stock name : " + si.getName() + " ,size : " + stList.size());
			}else {
				logger.info("------------>unmatch : " + stList.size());
			}
			logger.info("count : " + count++);
		}
		return results;
	}
	@InitBinder
    protected void initBinder(HttpServletRequest request,
    	    ServletRequestDataBinder binder) throws Exception {
    	    DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    	    CustomDateEditor dateEditor = new CustomDateEditor(fmt, true);
    	    binder.registerCustomEditor(Date.class, dateEditor);
    }
}
