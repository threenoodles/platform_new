/**
 * @FileName: IndexController.java
 * @Package com.cta.controller
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 * @version V1.0
 */
package com.cta.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cta.service.impl.ArticleService;

/**
 * @ClassName: PromotionController
 * @Description:
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 * 
 */
@Controller
@RequestMapping("/wm")
public class WMController {

	Logger logger = Logger.getLogger(WMController.class);

	@Resource
	private ArticleService articleService;
	@RequestMapping("portal")
	public String list(ModelMap model) throws IOException, SQLException {
		
		return "index/wmPortal";
	}
	
	@RequestMapping("shake")
	public String basklist(ModelMap model) throws IOException, SQLException {
		
		return "index/wmShake";
	}
	
	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("netty")
	public String index(ModelMap model) throws IOException, SQLException {
		return "/index/netty";
	}
}
