/**
 * @FileName: IndexController.java
 * @Package com.cta.controller
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 * @version V1.0
 */
package com.cta.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cta.entity.ArticleEntity;
import com.cta.platform.config.SystemGlobals;
import com.cta.platform.util.ListPager;
import com.cta.service.impl.ArticleService;
import com.cta.service.impl.PushService;
import com.cta.utils.DateTimeUtil;
import com.cta.utils.JSONUtil;

/**
 * @ClassName: IndexController
 * @Description:
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 * 
 */
@Controller
@RequestMapping("/article")
public class IndexController {

	public static Logger logger = Logger.getLogger(IndexController.class);

	@Resource
	private ArticleService articleService;

	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("list")
	public String list(
			ModelMap model,
			@RequestParam(value = "type_lang", defaultValue = "") Integer typeLang,
			@RequestParam(value = "type_cate", defaultValue = "") Integer typeCate,
			@RequestParam(value = "start", defaultValue = "0") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit)
			throws IOException, SQLException {

		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);

		if (typeLang == null) {
			typeLang = SystemGlobals.getIntPreference("article.type.all.id", 9);
		}
		if (typeCate == null) {
			typeCate = SystemGlobals.getIntPreference("article.lang.all.id", 3);
		}

		List<ArticleEntity> articleList = articleService
				.getArticleListByCateAndLang(typeLang, typeCate, pager);
		List<Map<String, Object>> cateList = articleService
				.getArticlCateListByStatus(ArticleEntity.STATUS_ENTABLED);
		List<Map<String, Object>> langList = articleService
				.getArticleLangListByStatus(ArticleEntity.STATUS_ENTABLED);

		model.put("articleList", articleList);
		model.put("typeCate", typeCate);
		model.put("typeLang", typeLang);
		model.put("cateList", cateList);
		model.put("langList", langList);
		model.put("start", start);
		model.put("limit", limit);

		return "/index/articleList";
	}

	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @param start
	 * @param limit
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("page")
	public String page(
			ModelMap model,
			@RequestParam(value = "type_lang", defaultValue = "") Integer typeLang,
			@RequestParam(value = "type_cate", defaultValue = "") Integer typeCate,
			@RequestParam(value = "start", defaultValue = "1") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit)
			throws IOException, SQLException {

		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);

		if (typeLang == null) {
			typeLang = SystemGlobals.getIntPreference("article.type.all.id", 9);
		}
		if (typeCate == null) {
			typeCate = SystemGlobals.getIntPreference("article.lang.all.id", 3);
		}

		List<ArticleEntity> articleList = articleService
				.getArticleListByCateAndLang(typeLang, typeCate, pager);

		model.put("articleList", articleList);
		model.put("start", start);
		model.put("limit", limit);

		return "jsonView";
	}

	/**
	 * @param model
	 * @param aid
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("/{aid}")
	public String article(ModelMap model, @PathVariable Long aid)
			throws IOException, SQLException {

		Map<String, Object> article = articleService.getDetailById(aid);
		model.put("article", article);

		return "jsonView";
	}

	/**
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("/new")
	@ResponseBody
	public void newArticle() throws IOException, SQLException {
		List<ArticleEntity> newArticleList = articleService
				.getArticleByStatus(ArticleEntity.STATUS_PUSH);
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 0, length = newArticleList.size(); i < length; i++) {
			ArticleEntity article = newArticleList.get(i);
			JSONObject _article = new JSONObject();
			_article.put("id", article.getId());
			_article.put("title", article.getTitle());
			_article.put("summary", article.getSummary());
			_article.put("time",
					DateTimeUtil.formatTime(article.getTime(), null));
			sb.append(_article.toString());
			if (i < length - 1) {
				sb.append(",");
			}
		}
		sb.append("]");

		logger.info(sb.toString().replace("\n", ""));

		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("action", 2);
		result.put("content", sb.toString().replace("\n", ""));

		OutputStream out = null;
		BufferedWriter writer = null;
		Map<Long, Socket> sockets = PushService.socketContainer;
		Iterator<Entry<Long, Socket>> iterators = sockets.entrySet().iterator();
		Map.Entry<Long, Socket> entry = null;
		Socket socket = null;
		while (iterators.hasNext()) {
			try {
				entry = iterators.next();
				socket = entry.getValue();
				out = socket.getOutputStream();
				writer = new BufferedWriter(
						new OutputStreamWriter(out, "UTF-8"));
				writer.write(result.toString() + "\n");
				writer.flush();
			} catch (Exception e) {
				logger.error("push article error : method->[newArticle] " + e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/connection/init")
	public void connection() throws IOException {
		Socket socket = null;
		BufferedWriter writer = null;
		OutputStream out = null;
		InputStream in = null;
		BufferedReader reader = null;
		try {// 创建一个Socket对象，并指定服务端的IP及端口号
			socket = new Socket(SystemGlobals.getPreference("push.server.ip"),
					SystemGlobals.getIntPreference("push.server.port", 1988));
			// 获取Socket的OutputStream对象用于发送数据。
			out = socket.getOutputStream();
			String socketData = "{uid:1,action:1,latestaid:10}\n";
			writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
			writer.write(socketData);
			writer.flush();

			while (true) {
				in = socket.getInputStream();
				reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
				String inputStr = reader.readLine();
				JSONObject result = JSONUtil.parseJSONObject(inputStr);
				if (result != null && result.getBoolean("success")
						&& (result.getInt("action") == 2)) {
					String articleStr = result.getString("content");
					JSONArray articles = JSONUtil.parseJSONArray(articleStr,
							true);
					if (articles != null) {
						System.out.println(String.valueOf(articles.size())
								+ articles.get(0));
					}
				} else if (result != null && result.getBoolean("success")
						&& (result.getInt("action") == 1)) {

					logger.info(result == null ? result : result.toString());
				} else {
					logger.info(result == null ? result : result.toString());
				}
				logger.info("sleeping...");

				Thread.sleep(1000l);
			}
		} catch (Exception e) {

			logger.error("");
			e.printStackTrace();

		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			if (writer != null) {
				writer.close();
			}
			if (reader != null) {
				reader.close();
			}

			if (socket != null) {
				socket.close();
			}
		}
		// return "success";
	}

	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("dataList")
	public String dataList(ModelMap model,
			@RequestParam(value = "start", defaultValue = "0") Integer start,
			@RequestParam(value = "limit", defaultValue = "5") Integer limit)
			throws IOException, SQLException {

		ListPager pager = new ListPager();
		Integer pageNo = (start / limit);
		pager.setRowsPerPage(limit);
		pager.setPageNo(pageNo);

		List<ArticleEntity> articleList = articleService
				.getArticleListByCateAndLang(9, 3, pager);

		model.put("articleList", articleList);
		model.put("start", start);
		model.put("limit", limit);

		return "/index/dataList";
	}
}
