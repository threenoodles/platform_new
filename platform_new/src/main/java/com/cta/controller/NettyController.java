/**
 * @FileName: IndexController.java
 * @Package com.cta.controller
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 * @version V1.0
 */
package com.cta.controller;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName: IndexController
 * @Description:
 * @author chenwenpeng
 * @date 2013-5-18 下午04:07:12
 * 
 */
@Controller
@RequestMapping("/netty")
public class NettyController {

	public static Logger logger = Logger.getLogger(NettyController.class);

	/**
	 * @param model
	 * @param typeLang
	 * @param typeCate
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping("")
	public String index(ModelMap model) throws IOException, SQLException {
		return "/index/netty";
	}

}
