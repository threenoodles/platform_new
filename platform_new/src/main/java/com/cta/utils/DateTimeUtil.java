package com.cta.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.cookie.DateParseException;

public class DateTimeUtil {

	private static Calendar calendar = Calendar.getInstance();
	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			DateTimeUtil.DEFAULT_FORMAT_DATE);
	private static byte[] lock = new byte[0];
	public static final String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
	public static final String DEFAULT_FORMAT_TIME = "HH:mm:ss";
	public static final String DEFAULT_FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final int DAY_INTERVAL_MS = 60 * 60 * 24 * 1000;
	/**
	 * 格式化时间
	 * 
	 * @param time
	 * @return
	 */
	public static String formatTime(Date time, String pattern) {
		if (time == null) {
			return StringUtils.EMPTY;
		}
		synchronized (lock) {
			if (pattern != null
					&& !DateTimeUtil.DEFAULT_FORMAT_DATE.equals(pattern)) {
				return new SimpleDateFormat(pattern).format(time);
			}
			return dateFormat.format(time);
		}
	}
	
	/**
	 * 格式化时间
	 * 
	 * @param time
	 * @return
	 */
	public static String formatTime(Date time) {
		if (time == null) {
			return StringUtils.EMPTY;
		}
		synchronized (lock) {
			return dateFormat.format(time);
		}
	}
	
	/**
	 * 解析时间
	 * 
	 * @param time
	 * @return
	 */
	public static Date parseTime(String dateTimeStr,String pattern) {

		if (StringUtils.isEmpty(dateTimeStr)) {
			return null;
		}

		try {
			DateFormat dateFormat = new SimpleDateFormat(pattern);
			return dateFormat.parse(dateTimeStr);
		} catch (Exception e) {
			return null;
		}
	
	}
	/**根据时间间隔获取 日期的间隔天数
	 * @param start
	 * @param end
	 * @return
	 * @throws DateParseException 
	 */
	public static int getDayInterval(Date start,Date end) throws DateParseException{
		if(null == start || null == end){
			throw new DateParseException("startDay or endDay can'not be null !");
		}else {
			Long count = (end.getTime() - start.getTime())/DAY_INTERVAL_MS;
			return count.intValue();
		}
	}
	
	public static void main(String[] args) throws DateParseException {
		System.out.println(DateTimeUtil.getDayInterval(DateTimeUtil.parseTime("2014-12-26", "yyyy-MM-dd"),new Date()));
	}
}
