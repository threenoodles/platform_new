package com.cta.utils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class JSONUtil {

	public static Logger logger = Logger.getLogger(JSONUtil.class);

	/**
	 * 根据jsonStr对应的jsonObject，如果异常返回null
	 * 
	 * @param jsonStr
	 * @param keys
	 * @return
	 */
	public static JSONObject parseJSONObject(String jsonStr) {

		try {
			if (StringUtils.isNotBlank(jsonStr)) {
				JSONObject json = JSONObject.fromObject(jsonStr);
				return json;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Json parse error : " + e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 根据jsonStr对应的jsonArray，如果异常返回null
	 * 
	 * @param jsonStr
	 * @param deepParse
	 *            是否需要进一步解析
	 * @return
	 */
	public static JSONArray parseJSONArray(String jsonStr, boolean deepParse) {

		try {
			if (StringUtils.isNotBlank(jsonStr)) {
				JSONArray jsonArr = JSONArray.fromObject(jsonStr);
				if (deepParse) {
					JSONArray objects = new JSONArray();
					String objStr = null;
					for (int i = 0, len = jsonArr.size(); i < len; i++) {
						objStr = jsonArr.getString(i);
						JSONObject object = JSONObject.fromObject(objStr);
						objects.add(object);
					}
					return objects;
				} else {
					return jsonArr;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Json parse error : " + e);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取成功的jsonobject
	 * 
	 * @return
	 */
	public static JSONObject getSuccessJson() {
		JSONObject json = new JSONObject();
		json.put("success", true);
		return json;
	}
}
