/**
 * @FileName: IndexDao.java
 * @Package com.cta.dao
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 ������04:11:09
 * @version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cta.entity.ArticleEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;
import com.cta.platform.util.ListPager;

/**
 * @author chenwenpeng
 * 
 */
@Repository
public class ArticleDao extends BaseDaoImpl {

	/**
	 * @param articleId
	 * @return
	 * @throws SQLException
	 */
	public Map<String, Object> getDetailById(Long articleId)
			throws SQLException {

		String sql = "select a.id,a.title,a.summary,a.type_lang typeLang,a.type_cate typeCate,a.time,d.content from j_article a,j_article_detail d where a.id = d.article_id and a.id = ?";

		return this.executeQueryOne(sql, new Long[] { articleId });
	}

	/**
	 * @param status
	 * @return
	 * @throws SQLException
	 */
	public List<ArticleEntity> getArticleByStatus(int status) throws SQLException {
		
		String sql = "select id,title,summary,time from j_article where status = ?";
		
		return this.selectAll(sql, new Integer[]{status}, ArticleEntity.class);
	}

	public List<ArticleEntity> getArticleListByCateAndLang(Integer typeLang,
			Integer typeCate, ListPager pager) throws SQLException {
		String sql = "select id,title,summary,time from j_article where type_lang = ? and type_cate = ? and status = ? order by id desc";
		
		return this.selectPage(sql, new Integer[]{typeLang,typeCate,ArticleEntity.STATUS_ENTABLED}, ArticleEntity.class,pager);
	}


	public List<Map<String, Object>> getArticlCateListByStatus(
			int statusEntabled) throws SQLException {
		
		String sql ="select id,name from j_cate where status = ?";
		return this.executeQuery(sql, new Integer[]{statusEntabled});
	}

	public List<Map<String, Object>> getArticleLangListByStatus(
			int statusEntabled) throws SQLException {
		
		String sql ="select id,name from j_lang where status = ?";
		return this.executeQuery(sql, new Integer[]{statusEntabled});
	}
}
