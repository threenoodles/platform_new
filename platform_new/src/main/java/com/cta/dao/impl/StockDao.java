/**
 * @FileName: IndexDao.java
 * @Package com.cta.dao
 * @Description: TODO
 * @author chenwenpeng
 * @date 2013-5-18 ������04:11:09
 * @version V1.0
 */
package com.cta.dao.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cta.entity.StockInfoEntity;
import com.cta.entity.StockTrendEntity;
import com.cta.platform.persistence.dao.BaseDaoImpl;

/**
 * @author chenwenpeng
 * 
 */
@Repository
public class StockDao extends BaseDaoImpl {

	/**
	 * @return
	 * @throws SQLException 
	 */
	public List<StockInfoEntity> getStockInfoList() throws SQLException {
		
		String sql = "select id,code,symbol,name,status from s_stock_info where status = ?";
		return this.selectAll(sql, new Integer[]{StockInfoEntity.STATUS_ENTABLED},StockInfoEntity.class);
	}

	public boolean existsStock(String code) throws SQLException {
		
		String sql = "select id,code,symbol,name,status from s_stock_info where symbol = ? limit 1";
		return this.selectOne(sql, new String[]{code}, StockInfoEntity.class) == null ? false : true;
	}

	public boolean existsStockTrend(String code, Date day) throws SQLException {
		
		String sql = "select id from s_stock_trend where code = ? and day = ? limit 1";
		return this.selectOne(sql, new Object[]{code,day}, StockInfoEntity.class) == null ? false : true;
	}
	
	public List<StockTrendEntity> getStockTrendByStockIdAndDayAndIP(Date startTime,
			Date endTime, int stockId, double increasePercent) throws SQLException {
		
		//select id,stock_id,code,volume,price_open,price_heigh,price_low,price_close,price_change,price_change_percent,turnrate,ma5,ma10,ma20,ma30,day from s_stock_trend where day >= '2014-12-01' and day <='2014-12-25' and price_change_percent >9 and code = 'SH600227' order by day;
		String sql = "select id,stock_id,code,volume,price_open,price_heigh,price_low,price_close,price_change,price_change_percent,turnrate,ma5,ma10,ma20,ma30,day from s_stock_trend where day >= ? and day <= ? and price_change_percent >? and stock_id = ? order by day";
		
		return this.selectAll(sql, new Object[]{startTime,endTime,increasePercent,stockId}, StockTrendEntity.class);
	
	}
}
