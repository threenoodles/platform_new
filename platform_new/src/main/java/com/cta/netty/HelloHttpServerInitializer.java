package com.cta.netty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;

public class HelloHttpServerInitializer extends ChannelInitializer<SocketChannel>{

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipline = ch.pipeline();
		pipline.addLast(new HttpServerCodec());
		//聚合http片段
		pipline.addLast(new HttpObjectAggregator(65536));
		//支持HTML5 文本传输
		pipline.addLast(new ChunkedWriteHandler());
		pipline.addLast(new HelloServerHandler());
	}
}
