package com.cta.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServer {

	/**
	 * 服务端监听的端口地址
	 */
	private static final int portNumber = 8379;

	public static void main(String[] args) {
		final EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workderGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap pushServer = new ServerBootstrap();
			pushServer.group(bossGroup, workderGroup)
					.channel(NioServerSocketChannel.class)// 用它来建立新accept的连接，用于构造serversocketchannel的工厂类
					.childHandler(new HelloServerInitializer());
			final Channel channel = pushServer.bind(portNumber).sync().channel();
			System.out.println("================Server started : " + portNumber+ "=================");
			channel.closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			bossGroup.shutdownGracefully();
			workderGroup.shutdownGracefully();
		}
	}
}
