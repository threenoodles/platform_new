package com.cta.netty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class HelloServerInitializer extends ChannelInitializer<SocketChannel>{

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipline = ch.pipeline();
//		pipline.addLast(new LineBasedFrameDecoder(1048));
		pipline.addLast(new StringDecoder());
		pipline.addLast(new StringEncoder());
		pipline.addLast(new HelloServerHandler());
	}
}
