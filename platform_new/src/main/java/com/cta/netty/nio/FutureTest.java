package com.cta.netty.nio;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ExecutorService executor = Executors.newCachedThreadPool();

		Runnable task1 = new Runnable() {
			@Override
			public void run() {
				System.out.println("I am task one");
			}
		};
		
		Callable<Integer> task2 = new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {

				System.out.println("I am task two");
				return null;
			}
		};
		
		Future<?> f1 = executor.submit(task1);
		Future<?> f2 = executor.submit(task2);
		System.out.println(f1.isDone());
		System.out.println(f2.isDone());
		
		
		while(f1.isDone()){
			System.out.println("=========task one done =========");
			break;
		}
		while(f2.isDone()){
			System.out.println("=========task two done =========");
			break;
		}
		
		
		String test ="A1234一二";
		byte[] bytes = test.getBytes();
		System.out.println(bytes);
	}
}
